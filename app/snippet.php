<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Snippet extends Model
{
    protected $fillable = ['name', 'selector_type', 'selector_value', 'container_type', 'container_value', 'video_settings', 'display_settings', 'generated_script'];
}
