<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionPlan extends Model
{
    protected $table = "transaction_plans";
    protected $fillable = ['user_id', 'plan_id', 'operation', 'apps_limit', 'customize', 'blacklist', 'action_tracking', 'report', 'email_support', 'phone_support', 'start_date', 'end_date', 'price'];
    public function plan()
    {
        return $this->hasOne('App\Plan', 'id','plan_id');
    }
    public function TransactionInvoices(){
    	return $this->hasOne('App\TransactionInvoices', 'transaction_id', 'id');
    }
}
