<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'remember_token', 'password', 'activation', 'company', 'website' , 'status', 'created_at', 'updated_at'
    ];
}
