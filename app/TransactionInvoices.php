<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionInvoices extends Model
{
    protected $table = "transaction_invoices";
    protected $fillable = ['transaction_id', 'amount', 'description'];
    public function transactionPlan()
    {
        return $this->belongsTo('App\TransactionPlan', 'transaction_id');
    }
}
