<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operations extends Model
{
    protected $table = "operations";
    protected $fillable = ['user_id', 'operation', 'usage', 'domains', 'start_date', 'end_date'];
}
