<?php

use App\User;
use Cookie as Cookie;
use Response as Response;

function sidemenu(){
	$mainmenu = array(
			'dashboard' => array(
						"Link" => "/",
						"Title" => "Dashboard",
						"Icon" => "fa fa-dashboard",
						"adminonly" => false
						),
			'apps' => array(
						"Link" => "/application",
						"Title" => "Application",
						"Icon" => "fa fa-globe",
						"adminonly" => false
						),
			'analytics' => array(
						"Link" => "/analytics",
						"Title" => "Analytics",
						"Icon" => "fa fa-area-chart",
						"adminonly" => false
						),
			'plan' => array(
						"Link" => "/plan",
						"Title" => "Plans Management",
						"Icon" => "fa fa-rocket",
						"adminonly" => true
						),
			'user' => array(
						"Link" => "/user",
						"Title" => "Manage User",
						"Icon" => "fa fa-users",
						"adminonly" => true
						),
			'transaction' => array(
						"Link" => "/transaction",
						"Title" => "Manage Transaction",
						"Icon" => "fa fa-book",
						"adminonly" => true
						),
			'blacklist' => array(
						"Link" => "/blacklist",
						"Title" => "Blacklist Videos",
						"Icon" => "fa fa-minus-circle",
						"adminonly" => false
						)
					);
	return $mainmenu;
}
function getsubheader($mainmenu){
	switch ($mainmenu) {
		// case 'apps':
		// 	$menu = array(
		// 			'Profile' => array(
		// 						"Link" => "/application",
		// 						"Title" => "Profile",
		// 						"Icon" => "fa fa-database"
		// 						),
		// 			'Snippet' => array(
		// 						"Link" => "/application/snippet",
		// 						"Title" => "Snippet",
		// 						"Icon" => "fa fa-cog"
		// 						),
		// 			'Display' => array(
		// 						"Link" => "/application/display",
		// 						"Title" => "Display",
		// 						"Icon" => "fa fa-tv"
		// 						)
		// 					);
		// 	return $menu;
		// 	break;
		case 'dashboard':
			$menu = array(
					'Dashboard' => array(
								"Link" => "/",
								"Title" => "Dashboard",
								"Icon" => "fa fa-database"
								),
					'AccountDetail' => array(
								"Link" => "/account/detail",
								"Title" => "Account Detail",
								"Icon" => "fa fa-newspaper-o"
								),
					'BillingDetail' => array(
								"Link" => "/account/billing",
								"Title" => "Billing Detail",
								"Icon" => "fa fa-credit-card"
								),
					'CurrentPlan' => array(
								"Link" => "/account/plan",
								"Title" => "Plan",
								"Icon" => "fa fa-line-chart"
								),
					'Invoices' => array(
								"Link" => "/account/invoice",
								"Title" => "Invoices",
								"Icon" => "fa fa-book"
								)
							);
			return $menu;
			break;
		default:
			return '';
			break;
	}
}

function getAllUser(){
	$users = User::all();
	return $users;
}

function LogasUser(){
	$usercookie = Cookie::get('logas');
	if ($usercookie != null & $usercookie != "") {
		$user = user::find($usercookie);
		return $user;
	}
	return Auth::user();
}

function resetcookie(){
	Cookie::queue(Cookie::forget('logas'));
	Cookie::queue(Cookie::forget('appid'));
	return true;
}