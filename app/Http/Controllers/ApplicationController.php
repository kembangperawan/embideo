<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Application;
use App\Plan;
use App\Snippet;
use App\Http\Requests;
use App\Http\Controllers\CryptoRandSecureController;
use App\Classes\ViewClass;
use File;
use Exception;
use URL;
use Response;
use Cookie;

class ApplicationController extends Controller
{
    public function getCurrentApp(){
        $appid = Cookie::get('appid');
        $application = null;
        if($appid != null && $appid != ""){
           $application = Application::find($appid);    
        }
        if ($application == null) {
            $application_list = Application::where('user_id', LogasUser()->id);
            $application = $application_list->first();
        }
        return $application;
    }
    public function index()
    {
        $application_list = Application::where('user_id', LogasUser()->id)->get();
        $application = $this->getCurrentApp();
        $snippet = New Snippet();
        $vfields = "";
        $dfields = "";
        if ($application != null) {
            $snippet = Snippet::find($application->snippet_id);
            $path = URL::asset("/static/ytsnippet.json");
            $json_string = file_get_contents($path);
            $json = json_decode($json_string, true);
            $fields = $json["fields"];            
            $vfields = $this->videosnippetjson($fields["video"], json_decode($snippet->video_settings));
            $dfields = $this->videosnippetjson($fields["display"], json_decode($snippet->display_settings));
        }
        $data = array('title' => 'Application',
    		'sidemenu' => 'apps',
            'application_list' => $application_list,
            'snippet' => $snippet,
            'application' => $application,
            'vfields' => $vfields,
            'dfields' => $dfields
    		);
    	return view('apps.index')->with($data);
    }
    public function create()
    {
        $data = array('title' => 'Application',
    		'sidemenu' => 'apps'
    		);
        return view('apps.application-update')->with($data);;
    }
    public function store(Request $request)
    {
        $snippet = New Snippet();
        $snippet->save();
        $application = New Application();
        $application->domain = $request->get('domain');
        $crypto = new CryptoRandSecureController;
        $token = $crypto->generate(32);
        $application->key = $token;
        $application->snippet_id = $snippet->id; 
        $application->user_id = Auth::user()->id;
        $application->name = $request->get('name');
        $application->description = $request->get('description');
        try {
            $application->save();
        } catch (Exception $e) {
            $snippet->destroy($snippet->id);
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return redirect('/application');
    }
    public function show($id)
    {
        $application = Application::find($id);
        $plan = Plan::find($application->plan_id);
        if ($application) 
        {
            return $application;
        }
        return redirect('/application');
    }
    public function edit($id)
    {
        if(!Auth::user())
        {
            return view('auth.login', ['message' => 'Not authorized to access this page']);
        }
        $application = Application::find($id);
        if ($application->user_id == Auth::user()->id)
        {
            $plan_list = Plan::all();
            return view('application-update', ['application' => $application, 'plan_list' => $plan_list]);
        } else {
            return 'Not authorized to update data';
        }
    }
    public function update(Request $request, $id)
    {
        $application = Application::find($id);

        if ($application)
        {
            $application->name = $request->get('name');
            $application->description = $request->get('description');
            $application->domain = $request->get('domain');
            $application->save();
            return redirect('/application');
        } else {
            return 'Not authorized to update data';
        }
    }
    public function destroy($id)
    {
        if(!Auth::user())
        {
            return view('auth.login', ['message' => 'Not authorized to access this page']);
        }
        $application = Application::find($id);
        if ($application)
        {
            $application->delete();
        }
        return redirect('/application');
    }
    public function selectorupdate(Request $request){
        $application = $this->getCurrentApp();
        $snippet = Snippet::find($application->snippet_id);
        $snippet->selector_type = $request->get("selectortype");
        $snippet->selector_value = $request->get("selectorvalue");
        $snippet->container_type = $request->get("containertype");
        $snippet->container_value = $request->get("containervalue");
        $snippet->update();
        $this->updatejs($application, $snippet);
        return redirect('/application');
    }
    public function queryupdate(Request $request){
        $application = $this->getCurrentApp();
        $snippet = Snippet::find($application->snippet_id);
        $snippet->prefix = $request->get("prefix");
        $snippet->sufix = $request->get("sufix");        
        $snippet->update();
        $this->updatejs($application, $snippet);
        return redirect('/application');
    }
    public function snippetupdate(Request $request)
    {
        $application = $this->getCurrentApp();
        $snippet = Snippet::find($application->snippet_id);
        $path = URL::asset("/static/ytsnippet.json");
        $json = json_decode(file_get_contents($path), true);
        $fields = $json["fields"];
        $vfields = $fields["video"];
        $jsonstring = "{";
        foreach ($vfields as $key => $value) {
            if ($request->get($value['name']) != null) {
                $jsonstring .= "\"" . $value['name'] . "\":"."\"" . $request->get($value['name']) . "\",";
            }
        }
        $jsonstring = substr($jsonstring,0,strlen($jsonstring)-1)."}";
        $snippet->video_settings = $jsonstring;
        $snippet->update();
        $this->updatejs($application, $snippet);
        return redirect('/application');
    }
    public function displayupdate(Request $request)
    {
        $application = $this->getCurrentApp();
        $snippet = Snippet::find($application->snippet_id);
        $path = URL::asset("/static/ytsnippet.json");
        $json = json_decode(file_get_contents($path), true);
        $fields = $json["fields"];
        $vfields = $fields["display"];
        $jsonstring = "{";
        foreach ($vfields as $key => $value) {
            if ($request->get($value['name']) != null) {
                $jsonstring .= "\"" . $value['name'] . "\":"."\"" . $request->get($value['name']) . "\",";
            }
        }
        $jsonstring = substr($jsonstring,0,strlen($jsonstring)-1)."}";
        $snippet->display_settings = $jsonstring;
        $snippet->update();
        $this->updatejs($application, $snippet);
        return redirect('/application');
    }
    public function videosnippetjson($snippetconfig, $snippetvalue){
        $data = [];
        $found = false;
        foreach ($snippetconfig as $key => $value) {
            if ($snippetvalue != null && $snippetvalue !="") {
                foreach ($snippetvalue as $key1 => $value2) {
                    if ($value["name"] == $key1) {
                        $value["value"] = $value2;
                        $data[$key] = $value;
                        $found = true;
                        break;
                    }
                }
            }
            if (!$found) {
                $value["value"] = "";
                $data[$key] = $value;
            }
            $found = false;
        }
        return $data;
    }
    public function change(Request $request){
        Cookie::queue('appid', $request->get('appid'), 43200);
        return redirect('/application');
    }
    public function regeneratekey(){
        $appid = Cookie::get('appid');
        $application = Application::find($appid);
        if ($application != null) {
            $crypto = new CryptoRandSecureController;
            $token = $crypto->generate(32);
            $application->key = $token;
            $application->update();
        }
        return redirect('/application');
    }
    public function updatejs($application, $snippet) {
        $templateResource = fopen(public_path().'\template.js', 'r') or die('Unable to open file!');
        $jsTemplate = fread($templateResource, filesize(public_path().'\template.js'));
        if(isset($snippet->video_settings)){
            $vs = json_decode($snippet->video_settings, true);
            $vs['type'] = 'video';
            $vs = json_encode($vs);
        }
        if(!is_null($snippet)){
            $jsTemplate = str_replace("SERVER_HOSTNAME", URL::asset("/"), $jsTemplate);
            $jsTemplate = str_replace("{{app_key}}", $application->key, $jsTemplate);
            $jsTemplate = str_replace("{{selector_type}}", isset($snippet->selector_type) ? $snippet->selector_type : "{{selector_type}}", $jsTemplate);
            $jsTemplate = str_replace("{{prefix_setting}}", isset($snippet->prefix) ? $snippet->prefix : "{{prefix_setting}}", $jsTemplate);
            $jsTemplate = str_replace("{{sufix_setting}}", isset($snippet->sufix) ? $snippet->sufix : "{{sufix_setting}}", $jsTemplate);
            $jsTemplate = str_replace("{{selector_value}}", isset($snippet->selector_value) ? $snippet->selector_value : "{{selector_value}}", $jsTemplate);
            $jsTemplate = str_replace("{{container_type}}", isset($snippet->container_type) ? $snippet->container_type : "{{container_type}}", $jsTemplate);
            $jsTemplate = str_replace("{{container_value}}", isset($snippet->container_value) ? $snippet->container_value : "{{container_value}}", $jsTemplate);
            $jsTemplate = str_replace("\"{{video_settings}}\"", isset($vs) ? $vs : "\"{{video_settings}}\"", $jsTemplate);
            $jsTemplate = str_replace("\"{{display_settings}}\"", isset($snippet->display_settings) ? $snippet->display_settings : "\"{{display_settings}}\"", $jsTemplate);
        }
        $userJsFile = fopen(public_path().'\\'.$application->key . '.js', 'w') or die('Unable to open file!');
        fwrite($userJsFile, $jsTemplate);
        fclose($templateResource);
        fclose($userJsFile);
    }
}
