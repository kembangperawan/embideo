<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Classes\ViewClass;
use App\TransactionInvoices;
use App\TransactionPlan;
use App\Plan;
use App\User;
use Auth;
use DB;
use PDF;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;


class AccountController extends Controller
{
    public function accountDetail(){
        $user = User::where('email', Auth::user()->email)->first();
    	$data = array('title' => 'Account',
    		'sidemenu' => 'dashboard',
    		'subactive' => 'AccountDetail',
            'user' => $user
    		);
    	return view('account.account-details')->with($data);
    }
    public function billingDetail(){
    	$data = array('title' => 'Billing Detail',
    		'sidemenu' => 'dashboard',
    		'subactive' => 'BillingDetail'
    		);
    	return view('account.account-billing-details')->with($data);
    }
	public function currentPlan(){
        $planList = Plan::all();
        $userPlan = TransactionPlan::select('plan_id')->where('user_id', Auth::user()->id)->orderBy('created_at')->first();
    	$data = array('title' => 'Plan',
    		'sidemenu' => 'dashboard',
    		'subactive' => 'CurrentPlan',
            'planlist' => $planList,
            'userPlan' => $userPlan
    		);
    	return view('account.account-current-plan')->with($data);
    }
	public function invoices(){
        $invoices_list = DB::table('transaction_plans')->select('transaction_invoices.id as invoiceid', 'amount', 'transaction_invoices.description', 'transaction_invoices.created_at as created_at')->join('transaction_invoices', 'transaction_plans.id', '=', 'transaction_invoices.transaction_id')->where('transaction_plans.user_id', Auth::user()->id)->get();
    	$data = array('title' => 'Invoices',
    		'sidemenu' => 'dashboard',
    		'subactive' => 'Invoices',
            'invoices_list' => $invoices_list
    		);
        return view('account.account-invoices')->with($data);
    }
    public function invoiceDetail($invoiceid){
        $invoice = DB::table('transaction_plans')->select('transaction_invoices.id as invoiceid', 'transaction_plans.id as transactionid','amount', 'transaction_invoices.description', 'transaction_invoices.created_at as created_at', 'users.name as name', 'users.email as email', 'userprofiles.company as company', 'userprofiles.website as website')->join('transaction_invoices', 'transaction_plans.id', '=', 'transaction_invoices.transaction_id')->join('users', 'transaction_plans.user_id', '=', 'users.id')->leftJoin('userprofiles', 'transaction_plans.id', '=', 'userprofiles.userid')->where('transaction_plans.user_id', Auth::user()->id)->where('transaction_invoices.id', $invoiceid)->first();
        $description = json_decode($invoice->description);
        $data = array('title' => 'Invoices',
            'sidemenu' => 'dashboard',
            'subactive' => 'Invoices',
            'invoice_detail' => $invoice,
            'description' => $description
            );
        return view('account.account-invoice-detail')->with($data);
    }
    public function printInvoiceDetail($invoiceid){
        $invoice = DB::table('transaction_plans')->select('transaction_invoices.id as invoiceid', 'transaction_plans.id as transactionid','amount', 'transaction_invoices.description', 'transaction_invoices.created_at as created_at', 'users.name as name', 'users.email as email', 'userprofiles.company as company', 'userprofiles.website as website')->join('transaction_invoices', 'transaction_plans.id', '=', 'transaction_invoices.transaction_id')->join('users', 'transaction_plans.user_id', '=', 'users.id')->leftJoin('userprofiles', 'transaction_plans.id', '=', 'userprofiles.userid')->where('transaction_plans.user_id', Auth::user()->id)->where('transaction_invoices.id', $invoiceid)->first();
        $description = json_decode($invoice->description);
        return view('account.account-invoice-detail-print')->with('invoice', $invoice)->with('description', $description)->with('type', 'print');
    }
    public function downloadInvoiceDetail($invoiceid){
        $invoice = DB::table('transaction_plans')->select('transaction_invoices.id as invoiceid', 'transaction_plans.id as transactionid','amount', 'transaction_invoices.description', 'transaction_invoices.created_at as created_at', 'users.name as name', 'users.email as email', 'userprofiles.company as company', 'userprofiles.website as website')->join('transaction_invoices', 'transaction_plans.id', '=', 'transaction_invoices.transaction_id')->join('users', 'transaction_plans.user_id', '=', 'users.id')->leftJoin('userprofiles', 'transaction_plans.id', '=', 'userprofiles.userid')->where('transaction_plans.user_id', Auth::user()->id)->where('transaction_invoices.id', $invoiceid)->first();
        $description = json_decode($invoice->description);
        $description = (array) $description;
        $invoice = (array) $invoice;
        $data = array( 'invoice' =>$invoice,
            'description' => $description,
            'type' => 'download'
            );
        $pdf = PDF::loadView('account.account-invoice-detail-print', compact('data'));
        return $pdf->stream('Invoice #'.$invoiceid.'.pdf');
    }
	public function update(Request $request){
		$user = User::where('email', Auth::user()->email)->first();	
		$validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email,'.$user->id,
            'name' => 'required|min:10|max:255'
        ]);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		}
		$user->update($user);
		return redirect("/account/detail")->with("message", "Successfuly updated your account.");
	}
	
	public function updatePassword(Request $request)
    {
        $user = User::where('email', Auth::user()->email)->first();
        $validator = Validator::make($request->all(),[
            'old_password' => 'required',
            'password' => 'required|alphaNum|between:6,16'
		]);

        if ($validator->fails()) {
            return redirect("/account/detail")->withErrors($validator);
        } else {
            if (!Hash::check(Input::get('old_password'), $user->password)) {
                return redirect("/account/detail")->withErrors(["message" =>'Your old password does not match']);
            } else {
                $user->password = Hash::make($request->input('password'));
                $user->save();
                return redirect("/account/detail")->with(["message"=>"Password have been changed"]);
            }
        }
    }
}
