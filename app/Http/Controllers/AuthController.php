<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;

class AuthController extends Controller
{
	/*
	status:
	0 : not set
	1 : true
	2 : false
	*/
	private $summary = array(
		'domain' => array(
						'status' => 0,
						'error' => ''
					),
		'usage' => array(
						'status' => 0,
						'error' => ''
					),
		'api' => array(
						'status' => 0,
						'error' => ''
					)
	);
	public function cekAll($url, $api_key) {
		$data = $this->cekAPI($api_key);
		if(!$data) {
			$this->summary['api']['status'] = 2;
			$this->summary['api']['error'] = 'API key error';
		} else {
			$this->summary['api']['status'] = 1;
		}
		$this->cekDomain($url, $data[0]->domains);

		$this->cekOperation($data[0]->operation, $data[0]->usages);
		$flag = 1;
		foreach ($this->summary as $key => $value) {
			if($value['status'] == 2) {
				$flag = 0;
				break;
			}
		}

		$sum = array(
			'stat' => $flag,
			'summary' => $this->summary
		);
		return $sum;
	}
    public function cekDomain($url, $domain) {
    	$dom_op = parse_url(str_replace('www.', '', $url));
    	$this->summary['domain']['status'] =  $dom_op['host'] == str_replace('www.', '', $domain) ? 1 : 2;
    	$this->summary['domain']['error'] = $dom_op['host'] == str_replace('www.', '', $domain) ? '' : 'Domain not allowed!';
    }
    public function cekOperation($op, $us) {
    	$this->summary['usage']['status'] = $op > $us ? 1 : 2;
    	$this->summary['usage']['error'] = $op > $us ? '' : 'Quota exceed limit';
    }
    public function cekAPI($api) {
    	$data = DB::select('select api_key, operation, usages, domains from operations 
        where api_key = :api_key',
        ['api_key' => $api]);
        return $data;
    }
}
