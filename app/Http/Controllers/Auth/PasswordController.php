<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\CryptoRandSecureController;
use App\Http\Controllers\EmailController;
use Hash;


class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    // use ResetsPasswords;

    // /**
    //  * Create a new password controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }
    public function resetPassword(){
        return view('auth.passwords.email');
    }
    public function sendPassword(Request $request){
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if($user == null){
            return redirect()->back()->with([
                'messageError' => "Username/email not found."
            ]);
        }
        if($this->sendEmailPassword($user)){
            return redirect('/auth/login')->with([
            'messageSuccess' => "New password sent. Please check your email."
            ]);    
        }
        return redirect()->back()->with([
            'messageError' => "Error accured. Please contact administrator."
        ]);
    }
    public function generateNewPassword(){
        $crypto = new CryptoRandSecureController;
        return $crypto->generate(8);
    }
    public function sendEmailPassword($user){
        $newPass = $this->generateNewPassword();
        $mail = new EmailController;
        $content = "<p>Hi ".$user->name.", </p>";
        $content .= "<p>Here's your new password.</p><br/>";
        $content .= "<div style='border: 1px solid #DDD; padding: 10px 5px;'><b>Password : </b><span style='color: #00BBF1;'>".$newPass."</span></div>";
        $content .= "<br/><p>Please change your password immediately for security reason.</p>";
        $user->password = Hash::make($newPass);
        $user->save();
        $mailObject = array(
            "subject" => "Embideo - Reset Password",
            "title" => "Embideo - Reset Password",
            "recipient" => $user->email,
            "content" => $content
        );
        return $mail->send($mailObject);
    }
}
