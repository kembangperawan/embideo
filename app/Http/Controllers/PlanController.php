<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Plan;
use Auth;
use App\Http\Requests;
use App\Classes\ViewClass;

class PlanController extends Controller
{
    public function index()
    {
        
        $plan_list = Plan::all();
        $data = array('title' => 'Plan Management',
    		'sidemenu' => 'plan',
            'plan_list' => $plan_list
    		);
        return view('plan.index')->with($data);
    }
    public function create()
    {
        $data = array('title' => 'Plan Management',
    		'sidemenu' => 'plan'
    		);
        return view('plan.plan-update')->with($data);
    }
    public function store(Request $request)
    {
        $plan = New Plan();
        $plan->name = $request->input('name');
        $plan->description = $request->input('description');
        $plan->operation = $request->input('operation');
        $plan->apps_limit = $request->input('apps_limit');
        $plan->customize = $request->input('customize');
        $plan->blacklist = $request->input('blacklist');
        $plan->action_tracking = $request->input('action_tracking');
        $plan->report = $request->input('report');
        $plan->email_support = $request->input('email_support');
        $plan->phone_support = $request->input('phone_support');
        $plan->price = $request->input('price'); 
        $plan->save();

        return redirect('/plan');
    }
    public function show($id)
    {
        $plan = Plan::find($id);
        if ($plan) 
        {
            return $plan;
        }
        return redirect('/plan');
    }
    public function edit($id)
    {
        $plan = Plan::find($id);
        $data = array('title' => 'Plan Management',
            'sidemenu' => 'plan',
            'plan' => $plan
            );
        return view('plan.plan-update')->with($data);
    }
    public function update(Request $request, $id)
    {
        $plan = Plan::find($id);

        if ($plan)
        {
            $plan->name = $request->input('name');
            $plan->description = $request->input('description');
            $plan->operation = $request->input('operation');
            $plan->apps_limit = $request->input('apps_limit');
            $plan->customize = $request->input('customize', 0);
            $plan->blacklist = $request->input('blacklist');
            $plan->action_tracking = $request->input('action_tracking');
            $plan->report = $request->input('report');
            $plan->email_support = $request->input('email_support');
            $plan->phone_support = $request->input('phone_support');
            $plan->price = $request->input('price'); 
            $plan->save();
            return redirect('/plan');
        }
        return redirect('/plan');
    }
    public function destroy($id)
    {
        $plan = Plan::find($id);
        if ($plan)
        {
            $plan->delete();
        }
        return redirect('/plan');
    }
}
