<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Mail;

class EmailController extends Controller
{
    public function send($mailobject)
    {
        // $title = $mailobject->$title;
        // $content = 'Welcome to Embideo!';
        $subject = $mailobject['subject'];
        $recipient = $mailobject['recipient'];
        Mail::send('emails.default', ['title' => $mailobject['title'], 'content' =>  $mailobject['content']], function ($message) use ($subject, $recipient)
        {
            $recipient = 'surabi.eman@gmail.com';
            $message->from('noreply@embideo.com', 'Embideo');
            $message->subject($subject);
            $message->to($recipient);
        });
        return true;
    }
}

// $message->from($address, $name = null);
// $message->sender($address, $name = null);
// $message->to($address, $name = null);
// $message->cc($address, $name = null);
// $message->bcc($address, $name = null);
// $message->replyTo($address, $name = null);
// $message->subject($subject);
// $message->priority($level);
// $message->attach($pathToFile, array $options = []);