<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TransactionPlan;
use Auth;
use App\Http\Requests;
use App\Classes\ViewClass;

class TransactionPlanController extends Controller
{
    public function index()
    {
        $transactionplan_list = TransactionPlan::all();
        $data = array('title' => 'Manage Transaction',
            'sidemenu' => 'transaction',
            'transactionlist' => $transactionplan_list
            );
        return view('transactionplan.index')->with($data);
        print_r($transactionplan_list);
    }
    public function create()
    {
        $data = array('title' => 'Manage Transaction',
            'sidemenu' => 'transaction'
            );
        return view('transactionplan.manage')->with($data);
    }
    public function store(Request $request)
    {
        $transactionplan = New TransactionPlan();
        $transactionplan->user_id = $request->input('user_id');
        $transactionplan->plan_id = $request->input('plan_id');
        $transactionplan->operation = $request->input('operation');
        $transactionplan->apps_limit = $request->input('apps_limit');
        $transactionplan->customize = $request->input('customize');
        $transactionplan->blacklist = $request->input('blacklist');
        $transactionplan->action_tracking = $request->input('action_tracking');
        $transactionplan->report = $request->input('report');
        $transactionplan->email_support = $request->input('email_support');
        $transactionplan->phone_support = $request->input('phone_support');
        $start_date = date_create( $request->input('start_date'));
        $end_date = date_create( $request->input('end_date'));
        $transactionplan->start_date = $start_date; 
        $transactionplan->end_date = $end_date; 
        $transactionplan->price = $request->input('price'); 
        $transactionplan->save();
        return redirect('/transaction');
    }
    public function show($id)
    {
        $transactionplan = TransactionPlan::find($id);
        if ($transactionplan) 
        {
            return $transactionplan;
        }
        return redirect('/transaction');
    }
    public function edit($id)
    {
        $transactionplan = TransactionPlan::find($id);
    }
    public function update(Request $request, $id)
    {
        $transactionplan = TransactionPlan::find($id);

        if ($transactionplan)
        {
            $transactionplan->name = $request->input('name');
            $transactionplan->description = $request->input('description');
            $transactionplan->operation = $request->input('operation');
            $transactionplan->apps_limit = $request->input('apps_limit');
            $transactionplan->customize = $request->input('customize', 0);
            $transactionplan->blacklist = $request->input('blacklist');
            $transactionplan->action_tracking = $request->input('action_tracking');
            $transactionplan->report = $request->input('report');
            $transactionplan->email_support = $request->input('email_support');
            $transactionplan->phone_support = $request->input('phone_support');
            $transactionplan->price = $request->input('price'); 
            $transactionplan->save();
            return redirect('/transaction');
        }
        return redirect('/transaction');
    }
    public function destroy($id)
    {
        $transactionplan = TransactionPlan::find($id);
        if ($transactionplan)
        {
            $transactionplan->delete();
        }
        return redirect('/transaction');
    }
}
