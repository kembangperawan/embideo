<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Snippet;
use Auth;
use App\User;
use App\Http\Requests;

class SnippetController extends Controller
{
    public function index()
    {
        $snippet_list = Snippet::all();
        return view('snippet', ['snippetList' => $snippet_list]);
    }
    public function create()
    {
        return view('snippet-update');
    }
    public function store(Request $request)
    {
        if(Auth::user())
        {
            return view('auth.login', ['message' => 'Not authorized to access this page']);
        }
        // Validate the request...
        $snippet_id = 0;
        if (!empty($request) && $request->get('id') != '')
        {
            $snippet_id = $request->get('id');
        }
        $snippet = New Snippet();
        if ($snippet_id > 0)
        {
            $snippet = Snippet::find($snippet_id);
            if (empty($snippet))
            {
                return 'Snippet not found';
            }
        }
        if (!isset($request)) {
            $snippet->name = $request->get('name');
            $snippet->selector_type = $request->get('selectorType');
            $snippet->selector_value = $request->get('selectorValue');
            $snippet->container_type = $request->get('containerType');
            $snippet->container_value = $request->get('containerValue');
            $snippet->video_settings = $request->get('videoSettings');
            $snippet->display_settings = $request->get('displaySettings');
            $snippet->generated_script = $request->get('generatedScript');
            $snippet->save();
        }
    }
    public function show(Request $request)
    {
        $snippet_id = 0;
        if (!isset($request) && $request->get('id') != '')
        {
            $snippet_id = $request->get('id');
        }
        if ($snippet_id > 0)
        {
            $snippet_id = Snippet::find($snippet_id);
        } else {
            $snippet_id = Snippet::all();
        }
    }
    public function edit()
    {
    }
    public function update()
    {
    }
    public function destroy(Id $id)
    {
        if(Auth::user())
        {
            return view('auth.login', ['message' => 'Not authorized to access this page']);
        }
        $snippet = Snippet::find($id);
        if (!isset($snippet))
        {
            $snippet->delete();
        }
        return redirect('/snippet');
    }
}
