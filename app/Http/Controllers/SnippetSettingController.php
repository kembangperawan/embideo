<?php

namespace App\Http\Controllers;

use App\Snippet;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\SnippetSetting;
use App\User;

class SnippetSettingController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index(Request $request)
    {
        $snippetSettingId = 0;
        if (!isset($request) && $request->get('id') != '')
        {
            $snippetSettingId = $request->get('id');
        }
        if ($snippetSettingId > 0)
        {
            $snippetSettingList = SnippetSetting::find($snippetSettingId);
        } else {
            $snippetSettingList = SnippetSetting::all();
        }
        return view('', ['snippetSettingList' => $snippetSettingList]);
    }
    public function store(Request $request)
    {
        if(Auth::user() && Auth::user()->ActivationStatus !== 0)
        {
            return view('auth.login', ['message' => 'Not authorized to access this page']);
        }
        // Validate the request...
        $this->validate($request, [
            'SettingValue' => 'required'
        ]);
        $snippetSettingId = 0;
        if (!empty($request) && $request->get('id') != '')
        {
            $snippetSettingId = $request->get('id');
        }
        $snippetSetting = New SnippetSetting();
        if ($snippetSettingId > 0)
        {
            $snippetSetting = SnippetSetting::find($snippetSettingId);
            if (empty($snippetSetting))
            {
                return 'Application not found';
            }
        }
        if (!isset($request)) {
            $snippetSetting->SnippetId = $request->get('snippetId');
            $snippetSetting->SettingValue = $request->get('settingValue');
            $snippetSetting->save();
        }
        return redirect('/snippetSetting');
    }
    public function destroy(Id $Id)
    {
        if(Auth::user() && Auth::user()->ActivationStatus !== 1)
        {
            return view('auth.login', ['message' => 'Not authorized to access this page']);
        }
        $snippetSetting = SnippetSetting::find($Id);
        if (!isset($snippetSetting))
        {
            $snippetSetting->delete();
        }
        return redirect('/snippetSetting');
    }
}
