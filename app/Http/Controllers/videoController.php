<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Controllers\CryptoRandSecureController;
use App\Http\Controllers\BigQueryController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlacklistVideoController;

use DB;
use App\Http\Requests;
use App\ApiKeys;
use App\SnippetSetting;
use App\Snippet;
use App\Application;
use Config;
use Google_Client;
use Google_Service_YouTube;


require_once 'Google/Client.php';

class VideoController extends Controller
{
    public function index(){
        $bq = new BigQueryController;
        $data = $this->apiCall(1);
        print_r($data);
        die();
        //ambil list of data transaction plan disini, lalu tampung dalam class transaction_plan 
        //data transaction plan akan dicompare dengan hasil embideo_log.call_embideo
        //setelah melakukan perhitungan, masukkan dalam table operations
        //process index ini akan dijalankan periodical contoh: 1 jam sekali atau 1 menit sekali
        //function index() rencanany akan dipanggil by schedule --> tbd, takutnya kalau ditaruh di index, bisa di hit dengan mudah, maybe apply index with random key?

        $transactionPlans = DB::select("SELECT transaction_plans.id, transaction_plans.user_id, transaction_plans.operation, transaction_plans.expected_start_date, transaction_plans.expected_end_date " . 
            "FROM transaction_plans " . 
            "where not exists ( " . 
            "select '' " . 
            "from transaction_plans temp " .
            "where temp.user_id = transaction_plans.user_id and temp.expected_start_date > transaction_plans.expected_start_date " .
            ") AND transaction_plans.expected_end_date > NOW();");
        $applications = Application::all();
        $operations = array();
        foreach ($transactionPlans as $transactionPlan) {
            $arrDomains = array();
            foreach($applications as $app) {
                if ($app->user_id == $transactionPlan->user_id) {
                    $tempArr = array(
                        "application_id" => $app->id,
                        "domain" => $app->domain
                    );
                    array_push($arrDomains, $tempArr);
                }
            }
            $operation = array(
                "user_id" => $transactionPlan->user_id,
                "operations" => $transactionPlan->operation,
                "usage" => 0,
                "domains" => json_encode($arrDomains),
                "start_date" => $transactionPlan->expected_start_date,
                "end_date" => $transactionPlan->expected_end_date,
            );
            array_push($operations, $operation);
        }

        $bq->stream('Embideo_Log', 'call_embideo_trial', $param);
        $bqDataList = $bq->select("SELECT user_id user_id, FORMAT_UTC_USEC(date_time) date_time FROM [Embideo_Log.call_embideo_trial] WHERE [valid_operation] = 1;", false);
        $calculatedOperationsData = '';
        foreach ($operations as $key => $opt) {
            $usageCount = 0;
            foreach($bqDataList as $bqData) {
                if ($bqData['user_id'] == $operations[$key]['user_id'] && (strtotime($bqData['date_time']) >= strtotime($operations[$key]['start_date']) && strtotime($bqData['date_time']) < strtotime($operations[$key]['end_date']))) {
                    $usageCount += 1;
                    $operations[$key]['usage'] = $usageCount;
                }
            }
            $calculatedOperationsData = $calculatedOperationsData . '(' . $operations[$key]['user_id'] . ',' . $operations[$key]['operations'] . ',' . $operations[$key]['usage'] . ', \'' . $operations[$key]['domains'] . '\',\'' . $operations[$key]['start_date'] . '\',\'' . $operations[$key]['end_date'] . '\') ,';
        }
        DB::statement("CREATE TEMPORARY TABLE operations_temp (user_id INT, operation INT, `usage` INT, domains VARCHAR(500), start_date DATE, end_date DATE);");
        DB::statement("INSERT INTO operations_temp (user_id, operation, `usage`, domains, start_date, end_date) VALUES " . rtrim($calculatedOperationsData, ',') . "; ");
        DB::statement("INSERT INTO operations (user_id, operation, `usage`, domains, start_date, end_date) SELECT user_id, operation, `usage`, domains, start_date, end_date FROM operations_temp WHERE NOT EXISTS (SELECT '' FROM operations WHERE operations.user_id = operations_temp.user_id); ");
        DB::statement("UPDATE operations " . 
            "INNER JOIN operations_temp ON operations_temp.user_id = operations.user_id " . 
            "SET operations.operation = operations_temp.operation, " . 
            "operations.usage = operations_temp.usage, " . 
            "operations.domains = operations_temp.domains, " . 
            "operations.start_date = operations_temp.start_date, " . 
            "operations.end_date = operations_temp.end_date; ");
        DB::statement("DELETE FROM operations WHERE NOT EXISTS (SELECT '' FROM operations_temp WHERE operations_temp.user_id = operations.user_id); ");
        DB::statement("DROP TABLE operations_temp;");
    }


    public function apiCall($userid) {
        $bq = new BigQueryController;
        $totalAPICall = $bq->select("SELECT COUNT('') TotalAPICall FROM [embidev:Embideo_Log.call_embideo_trial] WHERE user_id = " . $userid . " AND valid_operation = 1 LIMIT 1000", false);
        return $totalAPICall[0]['TotalAPICall'];
    }
    public function videoPlayed($userid) {
        $bq = new BigQueryController;
        $bqVideoPlayed = $bq->select("SELECT COUNT(distinct action_log.video_id + action_log.e_id) AS VideoPlayed ".
            "FROM [embidev:Embideo_Log.call_embideo_trial] AS call_log ".
            "INNER JOIN [embidev:Embideo_Log.action_log_trial] AS action_log ON action_log.e_id = call_log.id ".
            "WHERE call_log.user_id = " . $userid . " AND ".
            "action_log.state = 'playing'", false);
        
        return $bqVideoPlayed[0]['VideoPlayed'];
    }
    public function avgVideo($userid) {
        $bq = new BigQueryController;
        $bqAVGVideo = $bq->select("SELECT ".
            "(SUM(play_duration) / SUM(video_length)) avgPlayRate ".
            "FROM ".
            "[embidev:Embideo_Log.view_latest_action] ".
            "WHERE ".
            "user_id = ". $userid .
            " GROUP BY user_id", false);
        return $bqAVGVideo[0]['avgPlayRate'];
    }
    public function uniqueVideo($userid) {
        $bq = new BigQueryController;
        $bqUniqueVideo = $bq->select("SELECT ".
            "COUNT(DISTINCT video_id) UniqueVideo ".
            "FROM [embidev:Embideo_Log.call_embideo_trial] ".
            "WHERE user_id = " . $userid, false);
        return $bqUniqueVideo[0]['UniqueVideo'];
    }
    public function show($param) {
        $data = json_decode($param, true);        
        $kind = $data['param']['kind'];

        $kind_exec = array(
            'initial' => function ($data) {

                $last_key = DB::select("SELECT id, api_key FROM last_keys ORDER BY id DESC LIMIT 1");
                if (is_null($last_key) || sizeof($last_key) == 0) {
                    $pos = 0;
                }
                $g_api_key = Config('google.api');
                $total_key = sizeof($g_api_key);
                $pos = is_null($last_key) || sizeof($last_key) == 0 ? 0 : $last_key[0]->id % $total_key;
                DB::insert("insert into last_keys (api_key, created_at) values ('" . $g_api_key[$pos] . "', NOW())");

                $crypto = new CryptoRandSecureController;
                $token = $crypto->generate(32);         
                $data['param']['url'] = str_replace('\\', '/', $data['param']['url']);
                $em_api = $data['param']['api'];
                $q = $data['param']['q'];
                $ip = $data['param']['ip'];
                $url = $data['param']['url'];
                foreach ($data['param']['vsetting'] as $key => $value) {
                    $g_param[$key] = $value;
                }
                $g_param['q'] = $q;

                $auth = new AuthController;
                // $attempt = $auth->cekAll($url, $em_api);
                // if($attempt['stat'] == 0) {
                //     exit;
                // }

                $bl = new BlacklistVideoController;
                $blacklist = $bl->getBlacklistbyQuery(1, $q);
                $ret = $this->google(Config('google.api')[$pos], $g_param, $token, $blacklist);

                echo(json_encode($ret));

                $param = array(
                    'id' => $token,
                    'user_id' => 1,
                    'application_id' => 1,
                    'api_key' => $em_api,
                    'g_api_key' => Config('google.api')[$pos],
                    'ip_address' => $ip,
                    'query' => $q,
                    'response' => json_encode($ret),
                    'url' => $url,
                    'video_id' => $ret['data'][0]['ret'][0]['id'],
                    'parent_video_id' => $ret['data'][0]['ret'][0]['id'],
                    'position' => 1,
                    'view' => 1,
                    'valid_operation' => 1,
                    'date_time' => date('Y-m-d H:i:s')
                );                
                $bq = new BigQueryController;
                $bq->stream('Embideo_Log', 'call_embideo_trial', $param);
            },
            'event' => function ($data) {
                $data['param']['url'] = str_replace('\\', '/', $data['param']['url']);
                $em_api = $data['param']['api'];
                $q = $data['param']['q'];
                $ip = $data['param']['ip'];
                $url = $data['param']['url'];
                $video_id = $data['param']['vid'];
                $parent_video_id = $data['param']['pvid'];
                $position = $data['param']['position'];

                
                $crypto = new CryptoRandSecureController;
                $token = $crypto->generate(32);
                $param = array(
                    'id' => $token,
                    'user_id' => 1,
                    'application_id' => 1,
                    'api_key' => $em_api,
                    //'g_api_key' => Config('google.api')[$pos], --> tidak disimpan lagi google api-nya karena tidak memanggil data google lagi
                    'ip_address' => $ip,
                    'query' => $q,
                    //'response' => json_encode($ret),
                    'url' => $url,
                    'video_id' => $video_id,
                    'parent_video_id' => $parent_video_id,
                    'position' => $position,
                    'view' => 1,
                    'valid_operation' => 0,
                    'date_time' => date('Y-m-d H:i:s')
                );                
                $bq = new BigQueryController;
                $bq->stream('Embideo_Log', 'call_embideo_trial', $param);
            },
            'player' => function ($data) {
                $data['param']['url'] = str_replace('\\', '/', $data['param']['url']);
                $em_api = $data['param']['api'];
                $ip = $data['param']['ip'];
                $url = $data['param']['url'];                
                $video_id = $data['param']['vid'];
                $state = $data['param']['state'];
                $duration = $data['param']['duration'];
                $length = $data['param']['length'];
                $e_id = $data['param']['eid'];
                
                $state_en = array(
                    '-1' => 'unstarted',
                    '0' => 'ended',
                    '1' => 'playing',
                    '2' => 'paused',
                    '3' => 'buffering',
                    '5' => 'video cued'
                );
                $state_text = $state_en[$state];
                
                $crypto = new CryptoRandSecureController;
                $token = $crypto->generate(32);
                $param = array(
                    'id' => $token,
                    'user_id' => 1,
                    'application_id' => 1,
                    'api_key' => $em_api,
                    //'g_api_key' => Config('google.api')[$pos], --> tidak disimpan lagi google api-nya karena tidak memanggil data google lagi
                    'ip_address' => $ip,
                    'url' => $url,
                    'video_id' => $video_id,
                    'state' => $state_text,
                    'play_duration' => intval($duration),
                    'video_length' => intval($length),
                    'date_time' => date('Y-m-d H:i:s'),
                    'e_id' => $e_id
                );
                $bq = new BigQueryController;
                $bq->stream('Embideo_Log', 'action_log_trial', $param);
            },
            'vote' => function ($data) {
                $data['param']['url'] = str_replace('\\', '/', $data['param']['url']);
                $em_api = $data['param']['api'];
                $ip = $data['param']['ip'];
                $url = $data['param']['url'];
                $q = $data['param']['q'];
                $video_id = $data['param']['vid'];
                $state = $data['param']['vote'];
                $e_id = $data['param']['eid'];
                
                $crypto = new CryptoRandSecureController;
                $token = $crypto->generate(32);
                $param = array(
                    'id' => $token,
                    'user_id' => 1,
                    'application_id' => 1,
                    'api_key' => $em_api,                    
                    'ip_address' => $ip,
                    'url' => $url,
                    'query' => $q,
                    'video_id' => $video_id,
                    'state' => $state,
                    'date_time' => date('Y-m-d H:i:s'),
                    'e_id' => $e_id
                );
                $bq = new BigQueryController;
                $bq->stream('Embideo_Log', 'vote_log_trial', $param);
            }
        );
        $kind_exec[$kind]($data);              
    }

    private function google($g_api_key, $param, $e_id, $blacklist) {
    	$client = new Google_Client();
    	$client->setDeveloperKey($g_api_key);
    	$youtube = new Google_Service_YouTube($client);

    	$result = array();
        $error = array();
        $g_video = array();
        $g_channel = array();
        $g_playlist =  array();

        $g_video_c = array();
        $g_channel_c = array();
        $g_playlist_c =  array();

    	try {
            $searchResponse = $youtube->search->listSearch('id,snippet', $param);
            foreach ($searchResponse['items'] as $searchResult) {
                $caught = 0;
                foreach ($blacklist as $key => $value) {
                    if($value == $searchResult['id']['videoId']) {
                        $caught = 1;
                        break;                 
                    }
                }

                if ($caught == 0) {
                    switch ($searchResult['id']['kind']) {
                        case 'youtube#video':
                            array_push($g_video_c, array(
                                'title' => $searchResult['snippet']['title'],
                                'id' => $searchResult['id']['videoId'],
                                'publishedAt' => $searchResult['snippet']['publishedAt'],
                                'channelTitle' => $searchResult['snippet']['channelTitle'],
                                'description' => $searchResult['snippet']['description'],
                                'thumbnails' => $searchResult['snippet']['thumbnails']['default']['url']
                            ));
                            break;
                        case 'youtube#channel':
                            array_push($g_channel_c, array(
                                'title' => $searchResult['snippet']['title'],
                                'id' => $searchResult['id']['channelId'],
                                'publishedAt' => $searchResult['snippet']['publishedAt'],
                                'channelTitle' => $searchResult['snippet']['channelTitle'],
                                'description' => $searchResult['snippet']['description'],
                                'thumbnails' => $searchResult['snippet']['thumbnails']['default']['url']
                            ));
                            break;
                        case 'youtube#playlist':
                            array_push($g_playlist_c, array(
                                'title' => $searchResult['snippet']['title'],
                                'id' => $searchResult['id']['playlistId'],
                                'publishedAt' => $searchResult['snippet']['publishedAt'],
                                'channelTitle' => $searchResult['snippet']['channelTitle'],
                                'description' => $searchResult['snippet']['description'],
                                'thumbnails' => $searchResult['snippet']['thumbnails']['default']['url']
                            ));
                            break;
                    }
                }
                
            }
        
        } catch (Google_Service_Exception $e) {
            array_push($error, htmlspecialchars($e->getMessage()));            
        } catch (Google_Exception $e) {
            array_push($error, htmlspecialchars($e->getMessage()));
        }

        $g_video = array('type' => 'video','ret' => $g_video_c);
        //$g_channel = array('type' => 'channel','ret' => $g_channel_c);
        //$g_playlist = array('type' => 'playlist','ret' => $g_playlist_c);

        $result = array(
            //'data' => array($g_video, $g_channel, $g_playlist),
            'e_id' => $e_id,
            'data' => array($g_video),
            'error' => $error
        );

    	return $result;
    }

    private function err($err) {
        $msg = json_encode(array(
            'data' => '',
            'error' => $err
        ));
        echo($msg);
    }
}
