<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Application;
use App\TransactionPlan;
use App\User;
use Auth;
use App\Http\Controllers\videoController;

class DashboardController extends Controller
{
    public function index(){
        $vc = new videoController;
        $apiSummary = array(
            'apicall' => $vc->apiCall(LogasUser()->id),
            'videoPlayed' => $vc->videoPlayed(LogasUser()->id),
            'avgVideo' => number_format((float)($vc->avgVideo(LogasUser()->id) * 100), 2, '.', ''),
            'uniqueVideo' => $vc->uniqueVideo(LogasUser()->id),
        );
        $application_list = Application::all();
        $currentPlan = $this->getCurrentPlan();
        $remaining = "";
        if ($currentPlan) {
            $remaining = floor((strtotime($currentPlan->end_date) - strtotime(date('Y-m-d H:i:s')))/(60*60*24));
            $date=date_create($currentPlan->start_date);
            $currentPlan->start_date = date_format($date, 'M d, Y');
        }
    	$data = array('title' => 'Dashboard',
                'sidemenu' => 'dashboard',
                'subactive' => 'Dashboard',
                'application_list' => $application_list,
                'currentplan' => $currentPlan,
                'remaining' => $remaining,
                'apiSummary' => $apiSummary
                );
        return view('dashboard.overview')->with($data);
    }
    
    public function getCurrentPlan(){
        $current = TransactionPlan::where("user_id", Auth::user()->id)->orderBy('start_date', 'desc')->first();
        return $current;
    }
}
