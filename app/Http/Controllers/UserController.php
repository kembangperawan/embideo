<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use App\User;
use Hash;
use Validator;
use Redirect;
use Auth;
use Cookie;
class UserController extends Controller
{
    public function __construct()
    {
        if(Auth::user()->is_admin) return redirect::to('/'); // home
    }
    public function index(){
        $users = User::all();
        $data = array('title' => 'Manage User',
    		'sidemenu' => 'user',
            'users' => $users
    		);
        return view('user.index')->with($data);
    }
    public function create(){
        $data = array('title' => 'Manage User',
    		'sidemenu' => 'user'
    		);
        return view('user.manage')->with($data);
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|max:255',
            'password' => 'required|min:10|max:50|alpha_num'
        ]);
        if ($validator->fails()) {
           return redirect()->back()->withErrors($validator)->withInput();
        }
        $user = New User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();
        return redirect('/user')->with(["message" => "Successfuly created new user."]);
    }
    public function show(){
        
    }
    public function edit($id){
        $user = User::where("id", $id)->first();
        $data = array('title' => 'Manage User',
    		'sidemenu' => 'user',
            'user' => $user
    		);
        return view('user.manage')->with($data);
    }
    public function update(Request $request, $id){
        $user = User::where("id", $id)->first();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email,'.$user->id,
            'name' => 'required|max:255'
        ]);
        if ($validator->fails()) {
           return redirect()->back()->withErrors($validator)->withInput();
        }
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->update();
        return redirect('/user')->with(["message" => "Successfuly updated ".$user->email."."]);;
    }
    public function destroy($id){
        $user = user::find($id);
        if ($user)
        {
            $user->delete();
        }
        return redirect('/user')->with(["message" => "Successfuly deleted row."]);
    }
    public function setadmin($id){
        $user = user::find($id);
        if ($user)
        {
            $user->is_admin = !$user->is_admin;
            $user->update();
            return redirect('/user')->with(["message" => "Successfuly ". ($user->is_admin ? "set " : "remove ") . $user->name ." as admin."]);
        }
        return redirect('/user')->with(["message" => "Something wrong. Please try again."]);
    }
    public function logas(Request $request){
        $user = user::find($request->input('logasID'));
        $currentuser = Auth::user();
        $isadmin = $currentuser->is_admin;
        $message = "User not found or internal error";
        if($user != null && $isadmin && resetcookie()){
            if($user->id != $currentuser->id){
                Cookie::queue('logas', $user->id, 60);
            }
            $message = 'Logged as '.$user->email;
        }
        $response = array(
            "message" => $message
        );
        return json_encode($response);
    }
}
