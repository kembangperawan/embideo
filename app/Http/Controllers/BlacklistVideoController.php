<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\BlacklistVideo;
use App\Application;
use Validator;
Use DB;

class BlacklistVideoController extends Controller
{
    public function index()
    {
        $blacklists = BlacklistVideo::where('user_id', LogasUser()->id)->get();
        $data = array('title' => 'Blacklist Video',
    		'sidemenu' => 'blacklist',
            'blacklists' => $blacklists
    		);
    	return view('blacklist.index')->with($data);
    }
    public function getCurrentApp(){
        $appid = Cookie::get('appid');
        $application = null;
        if($appid != null && $appid != ""){
           $application = Application::find($appid);    
        }
        if ($application == null) {
            $application_list = Application::where('user_id', LogasUser()->id);
            $application = $application_list->first();
        }
        return $application;
    }
    public function create()
    {
        $application_list = Application::where('user_id', LogasUser()->id)->get();
        $data = array('title' => 'Blacklist Video',
    		'sidemenu' => 'blacklist',
            'application_list' => $application_list
    		);
    	return view('blacklist.create')->with($data);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_id' => 'required|url'
        ]);
        $parts = parse_url($request->video_id);
        parse_str($parts["query"], $query);
        $blacklist = new BlacklistVideo();
        $blacklist->video_id = $query['v'];
        $blacklist->application_id = $request->application_id;
        $blacklist->type = 1;
        $blacklist->user_id = LogasUser()->id;
        //blacklist type
        //1 = all
        //2 = specific
        $blacklist->save();
        return redirect('/blacklist')->with(["message" => "Successfuly created new blacklist video."]);
    }
    public function show(Request $request)
    {
        
    }
    public function edit()
    {
    }
    public function update()
    {
    }
    public function destroy($id)
    {
        $blacklist = BlacklistVideo::find($id);
        if ($blacklist)
        {
            $blacklist->delete();
        }
        return redirect('/blacklist')->with(["message" => "Successfuly removed blacklist video."]);
    }
    public function getBlacklist(){
        $application_id = $_GET['id'];
        $query = $_GET['q'];
        $this->getBlacklistbyQuery($application_id, $query);
    }
    public function getBlacklistbyQuery($application_id, $query){
        $blacklistAll = DB::select('select video_id from blacklist_videos 
        where application_id = :id AND is_active = 1 AND type = 1', 
        ['id' => $application_id]);
        $blacklistSpec = DB::select('select video_id from blacklist_videos 
        where application_id = :id AND query = :query AND is_active = 1', 
        ['id' => $application_id,
        'query' => $query]);
        $whitelist = DB::select('select video_id from blacklist_videos 
        where application_id = :id AND query = :query AND is_active = 0', 
        ['id' => $application_id,
        'query' => $query]);
        $result = [];
        foreach($blacklistAll as $key => $value){
            $isWhite = false;
            foreach($whitelist as $wKey => $wValue){
                if($wValue->video_id == $value->video_id){
                    $isWhite = true;
                    break;
                }
            }
            if(!$isWhite){
                $result[] = $wValue->video_id;
            }
        }
        foreach($blacklistSpec as $key => $value){
            $result[] = $value->video_id;
        }
        return $result;
    }
}
