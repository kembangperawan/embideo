<?php

namespace App\Http\Controllers;

use Config;
use Google_Client;
use Google_Auth_AssertionCredentials;
use Google_Service_Bigquery;
use Google_Service_Bigquery_QueryRequest;
use Google_Service_Bigquery_TableDataInsertAllRequestRows;
use Google_Service_Bigquery_TableDataInsertAllRequest;
use App\Application;
use DB;
use Cookie;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

require_once 'Google/Client.php';

class BigQueryController extends Controller
{
    public function getCurrentApp(){
        $appid = Cookie::get('appid');
        $application = null;
        if($appid != null && $appid != ""){
           $application = Application::find($appid);    
        }
        if ($application == null) {
            $application_list = Application::where('user_id', LogasUser()->id);
            $application = $application_list->first();
        }
        return $application;
    }
    public function index(){
        $application_list = Application::where('user_id', LogasUser()->id)->get();
        $currentApp = $this->getCurrentApp();
        $datenow = date('Y-m-d');
        $datenowend = date('Y-m-d', strtotime('+1 days'));
        $response = $this->defaultSelect($currentApp->id, strtotime($datenow), strtotime($datenow), strtotime($datenowend));
        // $appIDs = [];
        // $dataQuery = [];
        // foreach ($response as $key => $value) {
        //     $appIDs[count($appIDs)] = $value['application_id'];
        // }
        // $application = Application::whereIn('id', $appIDs)->get();
        // foreach ($response as $key => $value) {
        //     $resEach = $value;
        //     $found = false;
        //     foreach($application as $appkey => $appValue){
        //         if ($appValue["id"] == $value["application_id"]) {
        //             $resEach["application_name"] = $appValue["name"];
        //             $resEach["domain"] = $appValue["domain"];
        //             $found = true;
        //             break;
        //         }       
        //     }
        //     if (!$found) {
        //         $resEach["application_name"] = "";
        //         $resEach["domain"] = "";
        //     }
        //     $dataQuery[count($dataQuery)] = $resEach;
        // }
    	$data = array('title' => 'Analytics',
                'sidemenu' => 'analytics',
                'application_list' => $application_list,
                'application' => $currentApp,
                'response' => $response);
       return view('analytic.index')->with($data);
    }

    public function defaultSelect($applicationID, $startdate, $enddate){
        $query = "SELECT COUNT(*) as Count, Query, application_id, url 
            FROM [embidev:Embideo_Log.call_embideo_trial]
            WHERE application_id = ".$applicationID;
        // if($startdate != null && $enddate != null){
        //     $query .= " AND date_time>=".$startdate." AND date_time < ".$enddate;
        // }
        $query .=" GROUP By Query, application_id, url ORDER BY Count DESC";
        $response = $this->select($query, false);
        return $response;
    }

    public function getDefault(Request $request){
        $application_list = Application::where('user_id', LogasUser()->id)->get();
        $currentApp = $this->getCurrentApp();
        $datestart = $request->get('startdate');
        $dateend = $request->get('enddate');
        $response = $this->defaultSelect($currentApp->id, strtotime($datestart), strtotime($dateend));
    	$data = array('title' => 'Analytics',
                'sidemenu' => 'analytics',
                'application_list' => $application_list,
                'application' => $currentApp,
                'response' => $response);
       return view('analytic.index')->with($data);
    }
    
    public function select($query, $cache) {
        $bigquery = $this->auth();
        $request = new Google_Service_Bigquery_QueryRequest();

        $request->setQuery($query);
        
        //$request->setMaxResults($high);
        $request->setUseQueryCache($cache);
        
        $response = $bigquery->jobs->query(Config('google.project_id'), $request);
        $columns = $rows = array();
        // get columns...
        $fields = $response->getSchema()->getFields();
        foreach ($fields as $field) {
            $columns[] = $field->getName();
        }
        // ...then get rows
        foreach ($response->getRows() as $row) {
            $index = 0; // reset value
            $data = array(); // reset value
            foreach ($row->getF() as $k => $v) {
                $data[$columns[$index]] = $v->getV();
                $index++;
            }
            $rows[] = $data;
        }

        return $rows;
    }

    public function stream($dataset, $table, $param) {
        $bigquery = $this->auth();
        $data = $param;

        $rows = array();
        $row = new Google_Service_Bigquery_TableDataInsertAllRequestRows();
        $row->setJson($data);
        $rows[0] = $row;

        $request = new Google_Service_Bigquery_TableDataInsertAllRequest();
        $request->setKind('bigquery#tableDataInsertAllRequest');
        $request->setRows($rows);

        $options = array();
        $response = $bigquery->tabledata->insertAll(Config('google.project_id'), $dataset, $table, $request, $options);        
    }

    private function auth(){
        $client = new Google_Client();
        $client->setApplicationName("BigQuery");
        $client->setClientId(Config('google.client_id'));    

        if (isset($_SESSION['service_token'])) {
          $client->setAccessToken($_SESSION['service_token']);
        }
        $key = file_get_contents(Config('google.key_file_location'));
        $cred = new Google_Auth_AssertionCredentials(
            Config('google.service_account_name'),
            Google_Service_Bigquery::BIGQUERY,
            $key
        );

        $client->setAssertionCredentials($cred);
        if($client->getAuth()->isAccessTokenExpired()) {
          $client->getAuth()->refreshTokenWithAssertion($cred);
        }
        $_SESSION['service_token'] = $client->getAccessToken();

        $bigquery = new Google_Service_Bigquery($client);
        return $bigquery;
    }   

    public function showdetail($query){
        $data = array('title' => 'Analytics',
                'sidemenu' => 'analytics');
       return view('analytic.detail')->with($data);
    }
    public function change(Request $request){
        Cookie::queue('appid', $request->get('appid'), 43200);
        return redirect('/analytics');
    }
}
