<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the Routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


session_start();
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::post('/auth/password/email', 'Auth\PasswordController@sendPassword');
Route::get('/auth/password/reset', 'Auth\PasswordController@resetPassword');
Route::controllers([
    'auth' => 'Auth\AuthController'
]);
Route::get('account/activate', 'Auth\AuthController@activateAccount');
Route::post('user/log-as', 'UserController@logas');
Route::GET('/blacklist/get', 'BlacklistVideoController@getBlackList');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', "DashboardController@index");
    Route::get('/account/detail', 'AccountController@accountDetail');
    Route::get('/account/billing', 'AccountController@billingDetail');
    Route::get('/account/plan', 'AccountController@currentPlan');
    Route::get('/account/invoice', 'AccountController@invoices');
    Route::get('/account/invoice/{id}', 'AccountController@invoiceDetail');
    Route::get('/account/invoice/{id}/print', 'AccountController@printInvoiceDetail');
    Route::get('/account/invoice/{id}/download', 'AccountController@downloadInvoiceDetail');
    Route::get('/account/invoice/{id}/paymentconfirmation', 'PaymentConfirmationController@confirmPayment');
    Route::post('/account/invoice/confirm', 'PaymentConfirmationController@store');
    Route::post('/account/update', 'AccountController@update');
    Route::post('/account/password/reset', 'AccountController@updatePassword');
    Route::post('/query/update', "ApplicationController@queryupdate");
    Route::post('/snippet/update', "ApplicationController@snippetupdate");
    Route::post('/display/update', "ApplicationController@displayupdate");
    Route::post('/application/change', 'ApplicationController@change');
    Route::post('/application/regkey', 'ApplicationController@regeneratekey');
    Route::resource('snippet-setting', 'SnippetSettingController');
    Route::resource('/application', 'ApplicationController');
    Route::resource('/analytics', 'BigQueryController');
    Route::resource('/blacklist', 'BlacklistVideoController');
    Route::get('/analytics/show/{id}', 'BigQueryController@showdetail');
    Route::post('/analytics/change', 'BigQueryController@change');
    Route::post('/analytics/getdata', 'BigQueryController@getDefault');
});

Route::group(['middleware' => ['auth', 'App\Http\Middleware\RoleMiddleware']], function () {
    Route::resource('/plan', 'PlanController');   
    Route::resource('/user', 'UserController');
    Route::get('/user/setadmin/{id}','UserController@setadmin');
    Route::resource('/transaction', 'TransactionPlanController');
    Route::get('/transaction/generateinvoice/{id}','TransactionInvoicesController@generateInvoice');
});
Route::resource('v1', 'VideoController');
Route::get('/operations/calculate', 'VideoController@index');
Route::resource('/send', 'EmailController@send');