<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table = 'applications';
    protected $fillable = ['domain', 'key', 'snippet_id', 'user_id', 'name', 'description'];
}
