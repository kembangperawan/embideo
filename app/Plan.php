<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['name', 'description', 'operation', 'apps_limit', 'customize', 'blacklist', 'action_tracking', 'report', 'email_support', 'phone_support', 'price'];
}
