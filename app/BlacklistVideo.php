<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlacklistVideo extends Model
{
    protected $table = 'blacklist_videos';
}
