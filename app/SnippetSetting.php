<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SnippetSetting extends Model
{
    protected $table = 'SnippetSetting';
    protected $fillable = ['SnippetId', 'SettingValue'];
    public function Snippet(){
        return $this->belongsTo('App\Snippet');
    }
}
