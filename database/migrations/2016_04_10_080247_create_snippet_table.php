<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnippetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Snippet', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('ApplicationId');
            $table->string('Name', 255);
            $table->string('Domain', 255);
            $table->text('GeneratedScript');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Snippet');
    }
}
