<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('plan_id');
            $table->bigInteger('operation');
            $table->integer('apps_limit');
            $table->boolean('customize');
            $table->boolean('blacklist');
            $table->boolean('action_tracking');
            $table->string('report', 50);
            $table->string('email_support', 50);
            $table->boolean('phone_support');
            $table->date('start_date');
            $table->date('end_date');
            $table->decimal('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction_plans');
    }
}
