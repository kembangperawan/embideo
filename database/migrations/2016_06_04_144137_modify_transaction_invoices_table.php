<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTransactionInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_invoices', function (Blueprint $table) {
            $table->string('payment_status', 50);
            $table->decimal('amount', 12, 2)->change();
            $table->mediumtext('description')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_invoices', function (Blueprint $table) {
            //
        });
    }
}
