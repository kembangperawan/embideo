<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlacklistVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blacklist_videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('query');
            $table->string('video_id');
            $table->integer('application_id');
            $table->integer('user_id');
            $table->smallInteger('type');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blacklist_videos');
    }
}
