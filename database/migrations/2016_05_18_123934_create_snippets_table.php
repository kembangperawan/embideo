<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnippetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('snippets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('selector_type', 255);
            $table->string('selector_value', 255);
            $table->string('container_type', 255);
            $table->string('container_value', 255);
            $table->text('video_settings')->nullable();
            $table->text('display_settings')->nullable();
            $table->text('generated_script');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('snippets');
    }
}
