<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTransactionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_plans', function (Blueprint $table) {
            $table->decimal('price', 12, 2)->change();
            $table->date('expected_start_date')->after('end_date');
            $table->date('expected_end_date')->after('expected_start_date');
            $table->mediumtext('additional_feature')->after('expected_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_plans', function (Blueprint $table) {
            //
        });
    }
}
