window.embideo = window.embideo || function () {
	var _const = {
		ele : {
			type : "ID",
			value: "emb-source"
		}, 
		container : {
			type : "ID",
			value : "emb-container"
		}, 
		video_setting : {"safeSearch":"moderate","videoDefinition":"any","videoDimension":"2d","videoDuration":"any","type":"video"},
		display_setting : {"backgroundcolor":"#FFF","width":"720","height":"405"},
		query_setting : {
			prefix : "unboxing",
			sufix : ""
		},
		api : "EMo6mpkfuHPQrmAqWptXNtJz6tEcw9nT"
	};

	var ajax = {};
	ajax.x = function () {
	    if (typeof XMLHttpRequest !== 'undefined') {
	        return new XMLHttpRequest();
	    }
	    var versions = [
	        "MSXML2.XmlHttp.6.0",
	        "MSXML2.XmlHttp.5.0",
	        "MSXML2.XmlHttp.4.0",
	        "MSXML2.XmlHttp.3.0",
	        "MSXML2.XmlHttp.2.0",
	        "Microsoft.XmlHttp"
	    ];

	    var xhr;
	    for (var i = 0; i < versions.length; i++) {
	        try {
	            xhr = new ActiveXObject(versions[i]);
	            break;
	        } catch (e) {
	        }
	    }
	    return xhr;
	};

	ajax.send = function (url, callback, method, data, async) {
	    if (async === undefined) {
	        async = true;
	    }
	    var x = ajax.x();
	    x.open(method, url, async);
	    x.onreadystatechange = function () {
	        if (x.readyState == 4) {
	            callback(x.responseText)
	        }
	    };
	    if (method == 'POST') {
	        x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	    }
	    x.send(data)
	};

	ajax.get = function (url, data, callback, async) {
	    var query = [];
	    for (var key in data) {
	        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	    }
	    ajax.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async)
	};

	ajax.post = function (url, data, callback, async) {
	    var query = [];
	    for (var key in data) {
	        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	    }
	    ajax.send(url, callback, 'POST', query.join('&'), async)
	};

	function push(data) {		
		if(defined(data, "type")) {
			ajax.get('https://api.ipify.org?format=json', '', function(e) {
				var elvalue = _const.cases[_const.ele.type.toLowerCase()](_const.ele.value).tagName == "INPUT" ? 
					document.getElementById(_const.ele.value).value : document.getElementById(_const.ele.value).innerHTML;
				_const['ip'] = JSON.parse(e).ip;				
				_const['q'] = data.type == 'default' ? elvalue : defined(data, "q") && data.q != '' ? data.q : 'embideo';
				ajax.get('http://localhost:9999/embb/' + "v1/" + encodeURIComponent(JSON.stringify({
					param : {
						q : _const.query_setting.prefix + ' ' + _const['q'] + ' ' + _const.query_setting.sufix,
						api : _const.api,
						vsetting : _const.video_setting,
						url : "",
						ip : _const.ip,
						kind : 'initial'
					}				
				})), '', function (data) {
					pr(JSON.parse(data));
				});
			});
			
		}
	}

	function defined(target, path) {
	    if (typeof target != 'object' || target == null) {
	        return false;
	    }

	    var parts = path.split('.');

	    while(parts.length) {
	        var branch = parts.shift();
	        if (!(branch in target)) {
	            return false;
	        }

	        target = target[branch];
	    }

	    return true;
	}

	function init() {
		//create element
		var div = document.createElement('div')
			, up = document.createElement('span')
			, down = document.createElement('span')
			, prev = document.createElement('span')
			, next = document.createElement('span')
			, more = document.createElement('span')
			, title = document.createElement('div')
			, coll = document.createElement('div')
			, videoControl = document.createElement("div")
			, videoContainer = document.createElement("div")
			, videoThumbnail = document.createElement("div")
			, thumbnailBack = document.createElement("div");
		var controlStyle = "background: #FFF; cursor: pointer; margin-right: 5px;";
		
		//set attribute
		_const['player_id'] = 'emb_' + Math.round(100000 * Math.random());
		div.setAttribute("id", _const.player_id);
		coll.setAttribute("style", "background: " + _const.display_setting['backgroundcolor'] + "; padding: 10px; margin: auto;");
		title.setAttribute("style", "font-size: 20px; font-weight: bold; padding-bottom: 10px; margin-bottom: 10px; border-bottom: 1px solid #DDD;");
		videoContainer.setAttribute("style", "margin: auto; position: relative;");
		videoControl.setAttribute("style", "background: #FFF; margin: 5px 0;");
		prev.setAttribute("style", controlStyle);
		next.setAttribute("style", controlStyle);
		up.setAttribute("style", controlStyle);
		down.setAttribute("style", controlStyle);
		more.setAttribute("style", controlStyle);

		//overwrite html
		prev.innerHTML = "<img src='http://localhost:9999/embb/img/em-prev.png' alt='prev' title='Previous Video' />";
		next.innerHTML = "<img src='http://localhost:9999/embb/img/em-next.png' alt='next' title='Next Video' />";
		up.innerHTML = "<img src='http://localhost:9999/embb/img/em-up.png' alt='helpful' title='This video is helpful' />";
		down.innerHTML = "<img src='http://localhost:9999/embb/img/em-down.png' alt='not helpful' title='This video is not helpful' />";
		more.innerHTML = "<img src='http://localhost:9999/embb/img/em-more.png' alt='more videos' title='More videos' />";
		videoThumbnail.setAttribute("style", "padding: 10px 0; text-align: center;");
		videoControl.appendChild(title);
		videoControl.appendChild(more);
		videoControl.appendChild(prev);
		videoControl.appendChild(next);
		videoControl.appendChild(up);
		videoControl.appendChild(down);
		
		videoContainer.appendChild(div);
		videoContainer.appendChild(thumbnailBack);
		videoContainer.appendChild(videoControl);
		videoContainer.appendChild(videoThumbnail);
		

		coll.appendChild(videoContainer);
		
		//event declaration
		up.addEventListener('click', function() {
			console.log('up');
		});
		down.addEventListener('click', function() {
			console.log('down');
		});

		//up to constant
		_const['flag'] = 0;
		_const['collection'] = {
			'up' : up,
			'down' : down,
			'prev' : prev,
			'next' : next,
			'title' : title,
			'coll' : coll
		};
		_const['player_container'] = div;
		_const['video'] = {
			'videoControl' : videoControl,
			'videoContainer' : videoContainer,
			'videoThumbnail' : videoThumbnail,
			'thumbnailBack' : thumbnailBack
		};
		_const['cases'] = {
			"id" : function (s) {
				var val = document.getElementById(s);
				return val;
			},
			"class" : function (s) {
				var val = document.getElementsByClassName(s)[0];
				return val;
			}, 
			"tag" : function (s) {
				var val = document.getElementsByTagName(s)[0];
				return val;
			}
		};

		_const.cases[_const.container.type.toLowerCase()](_const.container.value).style.display = 'none';
		_const.cases[_const.container.type.toLowerCase()](_const.container.value).appendChild(coll);		
	}

	function pr(data) {	
		/*
			id => data.data[0].ret[_const['flag']].id
			title => data.data[0].ret[_const['flag']].title
			publishedAt => data.data[0].ret[_const['flag']].publishedAt
			channelTitle => data.data[0].ret[_const['flag']].channelTitle
			description => data.data[0].ret[_const['flag']].description
			thumbnails => data.data[0].ret[_const['flag']].thumbnails
		*/
		function thumb(data){

			for(var i=0; i< data.data[0].ret.length;i++){
				var thumbx = document.createElement("div");
				thumbx.setAttribute("style", "padding: 5px; cursor: pointer; width: 134px; display: inline-block; margin: 3px; border: 2px solid #DDD; background: #FFF;");
				thumbx.setAttribute("data-index", i);
				thumbx.setAttribute("title", data.data[0].ret[i].title);
				thumbx.innerHTML = "<img src='"+ data.data[0].ret[i].thumbnails +"' />";
				thumbx.innerHTML += "<div style='font-weight: bold; height: 40px; overflow: hidden;'>"+ data.data[0].ret[i].title +"</div>";
				_const['video'].videoThumbnail.innerHTML += thumbx.outerHTML;
			}
			for(var i=0; i <_const['video'].videoThumbnail.childNodes.length;i++){
				_const['video'].videoThumbnail.childNodes[i].addEventListener("click", function(){
					changeVid(this.getAttribute("data-index"));
				});
			}
		}
		function reload() {
			data.data[0].ret.length > 0 ?			
			_const.player.cueVideoById({
				videoId: data.data[0].ret[_const['flag']].id
			}) : "";
			_const.collection.title.innerHTML = data.data[0].ret[_const['flag']].title;
		}

		function setActive(position){
			for(var i=0;i<_const['video'].videoThumbnail.childNodes.length;i++){
				if(i==position)
					_const['video'].videoThumbnail.childNodes[i].setAttribute("style", "padding: 5px; cursor: pointer; width: 134px; display: inline-block; margin: 3px; border: 2px solid #4796E6; background: #FFF;");
				else
					_const['video'].videoThumbnail.childNodes[i].setAttribute("style", "padding: 5px; cursor: pointer; width: 134px; display: inline-block; margin: 3px; border: 2px solid #DDD; background: #FFF;");
			}	
		}

		function change(count) {
			var pos = parseInt(_const['flag']) + count;
			_const['flag'] = pos > data.data[0].ret.length - 1 ? 0 : pos < 0 ? data.data[0].ret.length - 1 : pos;
			_const.player.cueVideoById({
				videoId: data.data[0].ret[_const['flag']].id
			});
			ajax.get('http://localhost:9999/embb/' + "v1/" + encodeURIComponent(JSON.stringify({
				param : {
					q : _const.query_setting.prefix + ' ' + _const['q'] + ' ' + _const.query_setting.sufix,
					api : _const.api,
					url : "",
					ip : _const.ip,
					vid : data.data[0].ret[_const['flag']].id,
					pvid : data.data[0].ret[0].id,
					position: _const['flag'] + 1,
					kind : 'event'
				}				
			})), '', function(){});
			_const.collection.title.innerHTML = data.data[0].ret[_const['flag']].title;
			setActive(_const['flag']);
		}
		
		function changeVid(position) {
			_const['flag'] = position;
			_const.player.cueVideoById({
				videoId: data.data[0].ret[position].id
			});
			ajax.get('http://localhost:9999/embb/' + "v1/" + encodeURIComponent(JSON.stringify({
				param : {
					q : _const.query_setting.prefix + ' ' + _const['q'] + ' ' + _const.query_setting.sufix,
					api : _const.api,
					url : "",
					ip : _const.ip,
					vid : data.data[0].ret[position].id,
					pvid : data.data[0].ret[_const['flag']].id,
					position: position,
					kind : 'event'
				}				
			})), '', function(){});
			_const.collection.title.innerHTML = data.data[0].ret[_const['flag']].title;
			setActive(_const['flag']);
		}

		function onYouTubeIframeAPIReady() {
			_const['player'] = new YT.Player(_const.player_id, {
				height: 0,
				width: 0,
				videoId: data.data[0].ret[_const['flag']].id,
				events: {
					'onReady': onPlayerReady,
					'onStateChange': onPlayerStateChange
				}
			});
			thumb(data);
			setActive(_const['flag']);
			_const.collection.title.innerHTML = data.data[0].ret[_const['flag']].title;
		}

		function onPlayerReady(event) {
			_const.cases[_const.container.type.toLowerCase()](_const.container.value).style.display = 'block';
			setMediaSize();
		}

		function onPlayerStateChange(event) {
			ajax.get('http://localhost:9999/embb/' + "v1/" + encodeURIComponent(JSON.stringify({
				param : {
					api : _const.api,
					url : "",
					ip : _const.ip,
					vid : data.data[0].ret[_const['flag']].id,
					state : _const.player.getPlayerState(),
					duration : _const.player.getCurrentTime(),
					length : _const.player.getDuration(),
					kind : 'player',
					eid : data.e_id
				}
			})), '', function(){});
		}

		function stopVideo() {
			_const.player.stopVideo();
		}

		function setMediaSize(){
			var width =_const.cases[_const.container.type.toLowerCase()](_const.container.value).clientWidth * 0.9;
			var height = width * 9 / 16;
			_const['collection'].coll.setAttribute("style", _const['collection'].coll.getAttribute("style") + "width :" + width + "px;");
			document.getElementById(_const.player_id).setAttribute("width", width + "px");
			document.getElementById(_const.player_id).setAttribute("height", height + "px");
			_const['video'].thumbnailBack.setAttribute("style", "position: absolute; top: 0; left: 0; text-align: center; width: " + width + "px; height: " + height + "px; background: #000; opacity: 0.8;");
			_const['video'].videoThumbnail.setAttribute("style", "position: absolute; top: 0; left: 0; text-align: center; width: " + width + "px; height: " + height + "px;");
		}

		window.addEventListener("resize", function(){
			setMediaSize();
		});
		
		_const.collection.prev.addEventListener('click', function() {
			change(-1);
		});
		_const.collection.next.addEventListener('click', function() {
			change(1);
		});
		data.data[0].ret.length > 0 ? _const.player == '' || _const.player == undefined ? onYouTubeIframeAPIReady() : reload() : "";
	}
	return documentOnLoad = window.embideo && window.embideo.documentOnLoad ? window.embideo.documentOnLoad : function (t) {		
		t();
		var tag = document.createElement('script');
	    tag.src = "https://www.youtube.com/iframe_api";
	    tag.async = true;	    
	    var firstScriptTag = document.getElementsByTagName('script')[0];
    	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	}, {
		documentOnLoad: documentOnLoad,
		init: init,
		pr : pr,
		push : push
	};
}(), embideo.documentOnLoad(function () {
	embideo.init();
});


