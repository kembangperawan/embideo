@extends('layouts.default')
@section('title', 'Account Details')
@section('pageTitle', 'Account')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 col-xs-12">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>User ID</td>
                    <td>Plan Id</td>
                    <td>Name</td>
                    <td>Description</td>
                </tr>
            </thead>
            <tbody>
            @foreach($application_list as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->user_id }}</td>
                    <td>{{ $value->plan->name }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->description }}</td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>
                        <a class="btn btn-small btn-success" href="{{ URL::to('application/' . $value->id) }}">Show this application</a>
                        <a class="btn btn-small btn-info" href="{{ URL::to('application/' . $value->id . '/edit') }}">Edit this application</a>
                        <form method="POST" action="{{ 'http://localhost:9090/application/'.$value->id }}" accept-charset="UTF-8">
                            <input name="_method" type="hidden" value="DELETE" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" name='publish' class="btn btn-success" value="delete"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div><a href="{{ URL::to('application/create') }}">Create new application</a></div>
    </div>
  </div>
</section>
<!-- /.content -->
@stop
@section('page-script')
<script>
	$(function () {
		$('#datepicker').datepicker({
      		autoclose: true
    	});
	});
</script>
@stop
