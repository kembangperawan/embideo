@extends('layouts.default')
@section('page-stylesheet')
<link rel="stylesheet" href="{{ url('/plugins/datatables/dataTables.bootstrap.css') }}">
@stop
@section('content')
    <section class="content">
  		<div class="row">
  			<div class="col-md-8 col-md-offset-2 col-xs-12">
  				<div class="box">
		            <div class="box-body">
		              <table id="invoiceTable" class="table table-bordered table-striped">
		                <thead>
		                <tr>
		                  <th>Invoice Date</th>
		                  <th>Amount</th>
		                  <th>Description</th>
		                </tr>
		                </thead>
		                <tbody>
		                	 @foreach($invoices_list as $key => $value)
					            <tr>
					                <td><a href='{{ url('/account/invoice/'.$value->invoiceid) }}'>{{ $value->created_at }}</a></td>
					                <td>{{ 'Rp. '.number_format($value->amount, 2, ',', '.') }}</td>
					                <td>{{ $value->description }}</td>
					            </tr>
					        @endforeach
		                </tbody>
		              </table>
		            </div>
		            <!-- /.box-body -->
		          </div>
  			</div>
  		</div>
    </section>
@stop
@section('page-script')
<script src="{{ url('/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ url('/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script>
  $(function () {
     $('#invoiceTable').DataTable({
      "searching": false
    });
  });
</script>
@stop