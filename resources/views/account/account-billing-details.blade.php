@extends('layouts.default')
@section('page-stylesheet')
	
@stop
@section('content')
    <section class="content">
    	<div class="box">
    		<div class="box-body">
    			<div id="myBarChart" class="col-md-10"></div>
    		</div>
    	</div>
    </section>
@stop
@section('page-script')
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load('current', {packages: ['corechart']});
	  	google.charts.setOnLoadCallback(drawChart);
	    function drawChart() {
	      var data = google.visualization.arrayToDataTable([
	        ['Data', 'Used', 'Remaining'],
	        ['', 10, 100,]
	      ]);
	      var options = {
	          isStacked: 'percent',
	          height: 300,
	          legend: {position: 'top', maxLines: 3},
	          hAxis: {
	            minValue: 0,
	            ticks: [0, .3, .6, .9, 1]
	          }
	        };
	      var chart = new google.visualization.BarChart(document.getElementById('myBarChart'));
	      chart.draw(data, options);
	    }
	  </script>
@stop