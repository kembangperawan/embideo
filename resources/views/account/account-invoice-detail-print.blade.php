<!DOCTYPE html>
<html>
<head>
@include('includes.footer')
@if(isset($type) && $type == 'print')
  @include('includes.head')
@else
  <link rel="stylesheet" href="{{ URL::asset('/css/dashboard.css') }}">
  <style>
    .row {
      margin-right: -15px;
      margin-left: -15px;
    }
    .col-xs-12 {
      width: 100%;
      position: relative;
      min-height: 1px;
      padding-right: 15px;
      padding-left: 15px;
      float: left;
    }
    .col-sm-4 {
      position: relative;
      min-height: 1px;
      padding-right: 15px;
      padding-left: 15px;
      width: 33.33333333%;
    }
    .pull-right {
      float: right !important;
    }
    .row:after {
      clear: both;
    }
  </style>
@endif
</head>
<body>
<div class="wrapper">
  @if(isset($invoice) || isset($data))
  <section class="invoice">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> Embideo.Com
          @if(isset($invoice))
            <small class="pull-right">Date: {{$invoice->created_at}}</small>
          @else
            <small class="pull-right">Date: {{$data['invoice']['created_at']}}</small>
          @endif
        </h2>
      </div>
    </div>
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        From
        <address>
          <strong>Embideo.Com</strong><br>
              Email: admin@embideo.com
        </address>
      </div>
      <div class="col-sm-4 invoice-col">
        To
        <address>
          <strong>
            @if(isset($invoice))
              {{$invoice->name}}
            @else
              {{$data['invoice']['name']}}
            @endif
          </strong>
          @if(isset($invoice))
            @if(isset($invoice->company))
            <br>{{$invoice->company}}
            @endif
            @if(isset($invoice->website))
            <br> {{$invoice->website}}
            @endif
            <br>Email: {{$invoice->email}}
          @else
            @if(isset($data['invoice']['company']))
            <br>{{$data['invoice']['company']}}
            @endif
            @if(isset($data['invoice']['website']))
            <br> {{$data['invoice']['website']}}
            @endif
            <br>Email: {{$data['invoice']['email']}}
          @endif          
        </address>
      </div>
      <div class="col-sm-4 invoice-col">
        <b>Invoice #
          @if(isset($invoice))
            {{$invoice->created_at}}
          @else
            {{$data['invoice']['created_at']}}
          @endif
        </b><br>
        <b>Order ID:</b> 
        @if(isset($invoice))
          {{$invoice->transactionid}}
        @else
          {{$data['invoice']['transactionid']}}
        @endif
        <br>
      </div>
    </div>
    @if(isset($description) || isset($data))
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <tr>
            <td><strong>Operations</strong></td>
            <td>:</td>
            @if(isset($description))
              <td>{{$description->operation}}</td>
            @else
              <td>{{$data['description']['operation']}}</td>
            @endif
          </tr>
          <tr>
            <td><strong>Custom Looks</strong></td>
            <td>:</td>
            @if(isset($description))
              <td><input type="checkbox" disabled {{$description->customize == 0 ? "":"checked='checked'"}} /></td>
            @else
              <td><input type="checkbox" disabled {{$data['description']['customize'] == 0 ? "":"checked='checked'"}} /></td>
            @endif
          </tr>
          <tr>
            <td><strong>Blacklist</strong></td>
            <td>:</td>
            @if(isset($description))
              <td><input type="checkbox" disabled {{$description->blacklist == 0 ? "":"checked='checked'"}} /></td>
            @else
              <td><input type="checkbox" disabled {{$data['description']['blacklist'] == 0 ? "":"checked='checked'"}} /></td>
            @endif
          </tr>
          <tr>
            <td><strong>Track Actions</strong></td>
            <td>:</td>
            @if(isset($description))
              <td><input type="checkbox" disabled {{$description->action_tracking == 0 ? "":"checked='checked'"}} /></td>
            @else
              <td><input type="checkbox" disabled {{$data['description']['action_tracking'] == 0 ? "":"checked='checked'"}} /></td>
            @endif
          </tr>
          <tr>
            <td><strong>Report</strong></td>
            <td>:</td>
            @if(isset($description))
              <td>{{$description->report}}</td>
            @else
              <td>{{$data['description']['report']}}</td>
            @endif
          </tr>
          <tr>
            <td><strong>Email Support</strong></td>
            <td>:</td>
            @if(isset($description))
              <td>{{$description->email_support}}</td>
            @else
              <td>{{$data['description']['email_support']}}</td>
            @endif
          </tr>
          <tr>
            <td><strong>Phone Support</strong></td>
            <td>:</td>
            @if(isset($description))
              <td><input type="checkbox" disabled {{$description->phone_support == 0 ? "":"checked='checked'"}} /></td>
            @else
              <td><input type="checkbox" disabled {{$data['description']['phone_support'] == 0 ? "":"checked='checked'"}} /></td>
            @endif
          </tr>
        </table>
      </div>
    </div>
    @endif
    <div class="row">
      <div class="col-xs-6">
        <p class="lead">Payment Methods</p>
        <img src="{{URL::to('resources/assets/img/credit/paypal2.png')}}">
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
        </p>
      </div>
      <div class="col-xs-6">
        <p class="lead">Amount</p>
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Subtotal</th>
               @if(isset($invoice))
                <td style="text-align: right; margin-left: 20px;">{{'Rp. '.number_format($invoice->amount, 2, ',', '.')}}</td>
               @else
                <td style="text-align: right; margin-left: 20px;">{{'Rp. '.number_format($data['invoice']['amount'], 2, ',', '.')}}</td>
               @endif
            </tr>
            <tr>
              <th>VAT (10%)</th>
              @if(isset($invoice))
                <td style="text-align: right;">{{'Rp. '.number_format(($invoice->amount)*0.1, 2, ',', '.')}}</td>
              @else
                <td style="text-align: right;">{{'Rp. '.number_format(($data['invoice']['amount'])*0.1, 2, ',', '.')}}</td>
              @endif
            </tr>
            <tr>
              <th>Total</th>
              @if(isset($invoice))
                <td style="text-align: right;">{{'Rp. '.number_format($invoice->amount+(($invoice->amount)*0.1), 2, ',', '.')}}</td>
              @else
               <td style="text-align: right;">{{'Rp. '.number_format($data['invoice']['amount']+(($data['invoice']['amount'])*0.1), 2, ',', '.')}}</td>
              @endif
            </tr>
          </table>
        </div>
      </div>
    </div>
  </section>  
  @endif
</div>
</body>
</html>
@if(isset($type) && $type=='print')
<script type="text/javascript">
  $('document').ready(function() {
    window.print();
});  
</script>
@endif