@extends('layouts.default')
@section('content')
  <section class="content">
     <div class="box">
      <div class="box-body">
      @if(isset($invoice_detail))
        <div class="row">
          <div class="col-md-10 col-xs-12">
            <h2 class="page-header">
              Embideo
              <small class="pull-right">{{$invoice_detail->created_at}}</small>
            </h2>
          </div>
        </div>
        <div class="row invoice-info">
          <div class="col-sm-4 invoice-col">
            From
            <address>
              <strong>Embideo.Com</strong><br>
              Email: admin@embideo.com
            </address>
          </div>
          <div class="col-sm-4 invoice-col">
            To
            <address>
              <strong>{{$invoice_detail->name}}</strong>
              @if(isset($invoice_detail->company))
              <br>{{$invoice_detail->company}}
              @endif
              @if(isset($invoice_detail->website))
              <br> {{$invoice_detail->website}}
              @endif
            </address>
          </div>
          <div class="col-sm-4 invoice-col">
            <b>Invoice #{{$invoice_detail->invoiceid}}</b><br>
            <b>Order ID:</b> {{$invoice_detail->transactionid}}<br>
          </div>
        </div>
        @if(isset($description))
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <tr>
                <td><strong>Operations</strong></td>
                <td>:</td>
                <td>{{$description->operation}}</td>
              </tr>
              <tr>
                <td><strong>Custom Looks</strong></td>
                <td>:</td>
                <td><input type="checkbox" disabled {{$description->customize == 0 ? "":"checked='checked'"}} /></td>
              </tr>
              <tr>
                <td><strong>Blacklist</strong></td>
                <td>:</td>
                <td><input type="checkbox" disabled {{$description->blacklist == 0 ? "":"checked='checked'"}} /></td>
              </tr>
              <tr>
                <td><strong>Track Actions</strong></td>
                <td>:</td>
                <td><input type="checkbox" disabled {{$description->action_tracking == 0 ? "":"checked='checked'"}} /></td>
              </tr>
              <tr>
                <td><strong>Report</strong></td>
                <td>:</td>
                <td>{{$description->report}}</td>
              </tr>
              <tr>
                <td><strong>Email Support</strong></td>
                <td>:</td>
                <td>{{$description->email_support}}</td>
              </tr>
              <tr>
                <td><strong>Phone Support</strong></td>
                <td>:</td>
                <td><input type="checkbox" disabled {{$description->phone_support == 0 ? "":"checked='checked'"}} /></td>
              </tr>
            </table>
          </div>
        </div>
        @endif
        <div class="row">
          <div class="col-xs-6">
            <p class="lead">Payment Methods</p>
            <img src="{{URL::to('resources/assets/img/credit/paypal2.png')}}" alt="Paypal">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
              
            </p>
          </div>
          <div class="col-md-4 col-xs-6">
          <p class="lead">Amount</p>
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th style="width:50%">Subtotal</th>
                  <td style="text-align: right; margin-left: 20px;">{{'Rp. '.number_format($invoice_detail->amount, 2, ',', '.')}}</td>
                </tr>
                <tr>
                  <th>VAT (10%)</th>
                  <td style="text-align: right;">{{'Rp. '.number_format(($invoice_detail->amount)*0.1, 2, ',', '.')}}</td>
                </tr>
                <tr>
                  <th>Total</th>
                  <td style="text-align: right;">{{'Rp. '.number_format($invoice_detail->amount+(($invoice_detail->amount)*0.1), 2, ',', '.')}}</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="row no-print">
          <div class="col-xs-12">
            <a href="{{url('/account/invoice/'.$invoice_detail->invoiceid.'/print')}}" target='_blank' class="btn btn-default"><i class="fa fa-print"></i> Print</a>
            <a href="{{url('/account/invoice/'.$invoice_detail->invoiceid.'/paymentconfirmation')}}">
              <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Confirm Payment</button>
            </a>
            <a href="{{url('/account/invoice/'.$invoice_detail->invoiceid.'/download')}}">
              <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Generate PDF
              </button>
            </a>
          </div>
        </div>
      @endif
      </div>
    </div>
  </section>
@stop
      