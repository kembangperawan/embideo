@extends('layouts.default')
@section('title', 'Account Details')
@section('pageTitle', 'Account')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 col-xs-12">
    	<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
		        <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
		        <li><a href="#password" data-toggle="tab">Password</a></li>
		        <li><a href="#notifications" data-toggle="tab">Notifications</a></li>
            </ul>
            <div class="tab-content">
            	<div class="active tab-pane" id="profile">
	          		<div class="row">
		          		<div class="col-md-12">
							@if (Session::has("message"))
							<div class="alert alert-success">
								{{Session::get("message")}}
							</div>
							@endif
							@if($errors->has())
							<div class="alert alert-danger">
								{{$errors->first()}}
							</div>
							@endif
							<form class="form-horizontal" action="{{url('/account/update')}}" method="POST" enctype="multipart/form-data"> 
								{!! csrf_field() !!}
			          			<div class="form-group">
				                    <label for="email" class="col-sm-2 control-label">Email</label>
					                <div class="col-sm-10">
					                <input type="email" class="form-control" value="{{$user->email}}" id="email" name="email" placeholder="Email">
					                </div>
				                </div>
		                  		<div class="form-group">
				                    <label for="name" class="col-sm-2 control-label">Name</label>
				                    <div class="col-sm-10">
				                      <input type="text" class="form-control" value="{{$user->name}}" id="name" name="name" placeholder="Name">
				                    </div>
		                  		</div>
			                	<div class="form-group">
				                    <label for="company" class="col-sm-2 control-label">Company Name</label>
				                    <div class="col-sm-10">
				                      <input type="text" class="form-control" id="company" value="{{$user->company}}" name="company" placeholder="Company Name">
				                    </div>
			                    </div>
								<div class="form-group">
				                    <label for="website" class="col-sm-2 control-label">Website</label>
				                    <div class="col-sm-10">
				                      <input type="text" class="form-control" id="website" value="{{$user->website}}" name="website" placeholder="Website">
				                    </div>
			                    </div>
		                  		<div class="form-group">
		                    		<div class="col-sm-offset-2 col-sm-10">
		                      			<button type="submit" class="btn btn-info">Save Changes</button>
		                    		</div>
		                		</div>
	                		</form>
		          		</div>   			
	          		</div>
            	</div>
	            <div class="tab-pane" id="password">
	            	<div class="row">
		          		<div class="col-md-12">
		          			<form class="form-horizontal" method="POST" action="{{url('/account/password/reset')}}" enctype="multipart/form-data">
								{!! csrf_field() !!}

								@if (isset($message))
									<div class="alert alert-success">{{$message}}</div>
								@endif
								@if ($errors->has('message'))
									<div class="alert alert-danger">{{ $errors->first('message') }}</div>
								@endif
			            		<div class="form-group">
									<label for="old_password" class="col-sm-3 control-label">Current Password</label>
					                <div class="col-sm-9">
					                	<input type="password" class="form-control" id="old_password" name="old_password">
					                </div>
								</div>
								<div class="form-group">
									<label for="password" class="col-sm-3 control-label">New Password</label>
									<div class="col-sm-9">
										<input type="password" class="form-control" id="password" name="password">
									</div>
								</div>
								<div class="form-group">
									<label for="inputRetypeNewPassword" class="col-sm-3 control-label">Retype New Password</label>
									<div class="col-sm-9">
										<input type="password" class="form-control" id="retypePassword">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-8">
										<button type="submit" class="btn btn-info">Save Changes</button>
									</div>
								</div>
		            		</form>
		          		</div>
	          		</div>
	            </div>
            	<div class="tab-pane" id="notifications">
            		<div class="row">
		          		<div class="col-md-12">
		          			<form class="form-horizontal">
			            		<div class="form-group">
			            			<label class="col-sm-3 col-xs-6 control-label">Email Notification</label>
				                    <div class="col-sm-9 col-xs-6">
				                    	<input type="checkbox">
				                    </div>
			                  	</div>
			                  	<div class="form-group">
			            			<label class="col-sm-3 col-xs-6 control-label">Newsletter</label>
				                    <div class="col-sm-9 col-xs-6">
				                    	<input type="checkbox">
				                    </div>
			                  	</div>
			                  	<div class="form-group">
		                    		<div class="col-sm-offset-3 col-sm-8">
		                      			<button type="submit" class="btn btn-info">Save Changes</button>
		                    		</div>
		                		</div>
		            		</form>
		          		</div>
	          		</div>
            	</div>
            </div>
        </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop
@section('page-script')
<script>
	$(function () {
		$('#datepicker').datepicker({
      		autoclose: true
    	});
	});
</script>
@stop
