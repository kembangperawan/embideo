@extends('layouts.default')
@section('content')
    <section class="content">
    	 <div class="row">
    	 @if(isset($planlist))
    	 @foreach($planlist as $key => $value)
	        <div class="col-md-3 text-center">
	        	<div class="disabled" style="padding: 10px;">
	        		<div class="box box-widget widget-user-2">
		            <div class="widget-user-header bg-blue-active">
			            <h1 style="font-weight: bold;">{{$value->name}}</h1>
		            </div>
		            <div class="bg-olive-active" style="padding: 10px;">
			            Rp.<span style="font-size: 24px; padding: 0 5px; font-weight: bold;">{{number_format($value->price, 0, ',', '.')}}</span>/month
		            </div>
		            <div class="box-footer no-padding">
		              <ul class="nav nav-stacked">
		                <li>
		                	<div style="padding: 10px 0;">
		                		<div><h3>{{$value->operation}}</h3></div>
		                		<div>{{$value->operation>0?'Operations':'Operation'}}</div>
		                	</div>
		                </li>
		                <li>
			                <div style="padding: 10px 0;">
			                	<div><h3>{{$value->apps_limit}}</h3></div>
			                	<div>Application Limit</div>
		                	</div>
		                </li>
		                <li>
			                <div style="padding: 10px 0;">
			                	<div><h3>{{$value->customize}}</h3></div>
			                	<div>Custom Looks</div>
		                	</div>
		                </li>
		                <li>
			                <div style="padding: 10px 0;">
			                	<div><h3>{{$value->blacklist}}</h3></div>
			                	<div>Blacklist</div>
		                	</div>
		                </li>
		                <li>
			                <div style="padding: 10px 0;">
			                	<div><h3>{{$value->action_tracking}}</h3></div>
			                	<div>Track Actions</div>
		                	</div>
		                </li>
		                <li>
			                <div style="padding: 10px 0;">
			                	<div><h3>{{$value->email_support}}</h3></div>
			                	<div>Email Support</div>
		                	</div>
		                </li>
		                <li>
			                <div style="padding: 10px 0;">
			                	<div><h3>{{$value->phone_support}}</h3></div>
			                	<div>Phone Support</div>
		                	</div>
		                </li>
		              </ul>
		            </div>
		          </div>
		          <div>
		          @if($value->id <> $userPlan->plan_id)
		          	<button class="btn bg-orange" data-toggle="modal" data-target="#upgradeNotifModal" data-id="{{$value->name}}">Upgrade</button>
		          @else
		          	<button class="btn bg-orange disabled">Current Plan</button>
		          @endif
	              </div>
	        	</div>
	        </div>
		<div class="modal fade" id="upgradeNotifModal" tabindex="-1" role="dialog" aria-labelledby="upgradeNotifModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="upgradeNotifModalLabel">Upgrade Package</h4>
		      </div>
		      <div class="modal-body">
		        Are you sure to upgrade to <span id="packageID">Basic</span> Package?
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Upgrade</button>
		      </div>
		    </div>
		  </div>
		</div>
		@endforeach
		@endif
		</div>
    </section>
@stop
@section('page-script')
	<script type="text/javascript">
		$('#upgradeNotifModal').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget);
		  var id = button.data('id');
		  var modal = $(this);
		  modal.find('#packageID').text(id);
		})
	</script>
@stop
