@extends('layouts.default')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
      	<div class="col-xs-12">
            <h3>Confirm Payment</h3>
            @if($errors->has())
            <div class="alert alert-danger">{{$errors->first()}}</div>
            @endif
            <form method="POST" action="{{ url('/account/invoice/confirm') }}" accept-charset="UTF-8">
                <input name="_method" type="hidden" value="POST" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label>Invoice ID</label>
                    <input type="text" value="{{isset($invoice)?$invoice->id:''}}" name="invoice_id" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Payment Method</label>
                    <select class="form-control" name="payment_method">
                        <option value="1">Bank Transfer</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Amount</label>
                    <input value="{{isset($invoice)?$invoice->amount:''}}" type="text" name="amount" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Payment Date</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" required="required" name="payment_date" class="form-control pull-right" id="payment_date">
                    </div>
                </div>
                <div class="form-group">
                    <label>Note</label>
                    <textarea class="form-control" rows="3" name="note"></textarea>
                </div>
                <input type="submit" name='publish' class="btn btn-primary" value="SAVE"/>
                <input type="button" onclick="window.location='{{url('/account/invoice/'.$invoice->id)}}'" name='save' class="btn btn-default" value="Cancel" />
            </form>
        </div>
    </div>
</section>
@stop
@section('page-script')
<script type="text/javascript">
    $('#payment_date').datepicker({
      autoclose: true
    });
</script>
@stop