<html>
    <head>
        <title>{{$title}}</title>
    </head>
    <body style="margin: 0; padding: 10px; font-family: Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif; font-size: 14px; color: #333;">
        <table cellspacing="0" cellpadding="0" border="0" style="width: 500px; margin: auto; border: 1px solid #DDD;">
            <tr><td colspan="3">&nbsp;</td></tr>
            <tr><td>&nbsp;</td><td><a href="http://www.embideo.com"><img src="http://dev.embideo.com/img/embideo-logo.png" alt="Embideo" title="Embideo" width="85px" /></a></td><td>&nbsp;</td></tr>
            <tr><td colspan="3">&nbsp;</td></tr>
            <tr><td colspan="3">&nbsp;</td></tr>
            <tr>
                <td width="20px"></td>
                <td>
                    {!! $content !!}
                </td>
                <td width="20px"></td>
            </tr>
            <tr><td>&nbsp;</td><td><hr/></td><td>&nbsp;</td></tr>
            <tr>
                <td width="20px"></td>
                <td>
                    <p>
                        <a href="http://www.embideo.com" style="text-decoration: none;"><span style="color: #00BBF1; font-weight: bold;">embideo</span></a>
                        <span style="display: block; font-size: 12px; color: #abadae;">
                            Lt. 2 Gedung 50 Abdul Muis<br />
                            Gambir, Jakarta Pusat<br />
                            Indonesia<br />
                            +62 8122 1629 072
                        </span>
                    </p>
                </td>
                <td width="20px"></td>
            </tr>
        </table>
    </body>
</html>