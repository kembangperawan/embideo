@extends('layouts.default')
@section('content')
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
      	<div class="col-md-6 col-xs-12">
      		<div class="box box-default">
      			<div class="box-header"><h3 class="box-title">Search Query</h3></div>
      			<div class="box-body">
      				<div class="form-group">
      					<label>Prefix</label>
      					<input type="text" name="txtChannelID" class="form-control" />
      				</div>
      				<div class="form-group">
      					<label>Suffix</label>
      					<input type="text" name="txtChannelID" class="form-control" />
      				</div>
      				<div class="form-group">
      					<label>Contains</label>
      					<input type="text" name="txtChannelID" class="form-control" />
      				</div>
      				<div class="form-group">
      					<label>Video Caption</label>
      					<input type="text" name="txtChannelID" class="form-control" />
      				</div>
      			</div>
      		</div>
      		<div class="box box-default">
      			<div class="box-header"><h3 class="box-title">Video Setting</h3></div>
      			<div class="box-body">
      				<div class="form-group">
      					<label>Video Definition</label>
      					<select class="form-control">
      						<option>Any</option>
      						<option>HD</option>
      						<option>Standard</option>
      					</select>
      				</div>
      				<div class="form-group">
      					<label>Video Duration</label>
      					<select class="form-control">
      						<option>Any</option>
      						<option>Long</option>
      						<option>Medium</option>
      						<option>Short</option>
      					</select>
      				</div>
      			</div>
      		</div>
      	</div>
      	<div class="col-md-6 col-xs-12">
      		<div class="box box-default">
      			<div class="box-header"><h3 class="box-title">Show Option</h3></div>
      			<div class="box-body">
      				<div class="form-group">
      					<label>Order</label>
      					<select class="form-control">
      						<option>Relevance</option>
      						<option>Date</option>
      						<option>Rating</option>
      						<option>Title</option>
      						<option>Video Count</option>
      						<option>View Count</option>
      					</select>
      				</div>
      				<div class="form-group">
      					<label>Published Date</label>
      					<div class="input-group">
      					  <button type="button" class="btn btn-default pull-right" id="daterange-btn">
      					    <span>
      					      <i class="fa fa-calendar"></i> Select Date Range
      					    </span>
      					    <i class="fa fa-caret-down"></i>
      					  </button>
      					</div>
      				</div>
      			</div>
      		</div>
      		<div class="box box-default">
      			<div class="box-header"><h3 class="box-title">Channels and Types</h3></div>
      			<div class="box-body">
      				<div class="form-group">
      					<label>Channel ID</label>
      					<input type="text" name="txtChannelID" class="form-control" />
      				</div>
      				<div class="form-group">
      					<label>Channel Type</label>
      					<select class="form-control">
      						<option> - Select - </option>
      						<option>Any</option>
      						<option>Show</option>
      					</select>
      				</div>
      				<div class="form-group">
      					<label>Event Type</label>
      					<select class="form-control">
      						<option> - Select - </option>
      						<option>Completed</option>
      						<option>Live</option>
      						<option>UpComing</option>
      					</select>
      				</div>
      			</div>
      		</div>
      	</div>

      	<div class="col-xs-12 text-right">
      		<div class="form-group"><button class="btn btn-primary">SAVE SETTINGS</button></div>
      	</div>
      </div>
      <!-- /.row (main row) -->
    </section>
@stop