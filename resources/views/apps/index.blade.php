@extends('layouts.default')
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        @if (isset($application) && $application != null)
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div class="col-xs-6">
                <form action="{{URL('/application/change')}}" method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group">
                    <label style="font-size: 16px;">Profile&nbsp;</label>
                    <select class="form-control select2" onchange="this.form.submit()" style="width: 80%;"  name="appid">
                    @if(!is_null($application_list))
                      @foreach($application_list as $field => $value )
                        <option value="{{$value->id}}"{{$value->id == $application->id ? "selected='selected'" : ""}}>{{$value->name}}</option>
                      @endforeach
                    @endif
                    </select>
                  </div>
                </form>
              </div>
              <div class="col-xs-6 text-right">
                <button class="btn btn-primary" onclick="window.location='{{url('/application/create')}}'"><i class="fa fa-plus"></i>  Add New Application</button>
              </div>
              <div style="clear:both"></div>
              <div style="padding: 10px; background: #F6F6F6; margin: 10px 0;">
                {{ isset($application) ? $application->description : "" }}
              </div>
              <div style="margin-top: 20px;">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#preview" data-toggle="tab" aria-expanded="true">Preview</a></li>
                    <li><a href="#snippets" data-toggle="tab">Snippets</a></li>
                    <li><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li>
                    <li><a href="#apikeys" data-toggle="tab" aria-expanded="false">API Key</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="preview">
                      <div class="text-center form-inline" style="margin: 20px 0;">
                        <span id="spnPrefix">{{$snippet->prefix}}</span> <input type="text" name="embQueryMaster" value="iphone se" placeholder="Search something" style="width: 70%" class="form-control form-lg"> <span id="spnSufix">{{$snippet->sufix}}</span>                        
                      </div>
                      <h3>Result</h3>
                      <div id="embSelMaster" style="display: none;"><div {{$snippet->selector_type}}="{{$snippet->selector_value}}"></div></div>
                      <div {{$snippet->container_type}}="{{$snippet->container_value}}"></div>
                    </div>
                    <div class="tab-pane" id="snippets">
                      <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-xs-12">
                          <h3>Query <small>setting</small></h3>
                          <form method="POST" action="{{URL('/query/update')}}">
                            <input type="hidden" name="appid" value="{{ isset($application) ? $application->id : "" }}" /> 
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                              <label>Prefix</label>                              
                              <input type="text" max class="form-control" name="prefix" value="{{ isset($snippet) ? $snippet->prefix : "" }}" />
                            </div>
                            <div class="form-group">
                              <label>Sufix</label>
                              <input type="text" class="form-control" name="sufix" value="{{ isset($snippet) ? $snippet->sufix : "" }}" />
                            </div>
                            <div class="form-group">
                              <button class="btn btn-primary" type="submit">SAVE CHANGES</button>
                            </div>
                          </form>
                          <hr />
                          <h3>Video <small>selector</small></h3>
                          <form method="POST" action="{{URL('/selector/update')}}">
                            <input type="hidden" name="appid" value="{{ isset($application) ? $application->id : "" }}" /> 
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group form-inline">
                              <label>Query Target</label><br />
                              <select name="selectortype" class="form-control">
                                <option{{$snippet->selector_type == "ID" ? " selected='selected'" : ""}}>ID</option>
                                <option{{$snippet->selector_type == "Class" ? " selected='selected'" : ""}}>Class</option>
                                <option{{$snippet->selector_type == "Tag" ? " selected='selected'" : ""}}>Tag</option>
                              </select>
                              <input type="text" class="form-control" name="selectorvalue" value="{{ isset($snippet) ? $snippet->selector_value : "" }}" />
                            </div>
                            <div class="form-group form-inline">
                              <label>Container</label> <br />
                              <select name="containertype" class="form-control">
                                <option{{$snippet->container_type == "ID" ? " selected='selected'" : ""}}>ID</option>
                                <option{{$snippet->container_type == "Class" ? " selected='selected'" : ""}}>Class</option>
                                <option{{$snippet->container_type == "Tag" ? " selected='selected'" : ""}}>Tag</option>
                              </select>
                              <input type="text" class="form-control" name="containervalue" value="{{ isset($snippet) ? $snippet->container_value : "" }}" />
                            </div>
                            <div class="form-group">
                              <button class="btn btn-primary" type="submit">SAVE CHANGES</button>
                            </div>
                          </form>
                          <hr />
                          <h3>Video <small>snippet</small></h3>
                          <form method="POST" action="{{URL('/snippet/update')}}">
                            <input type="hidden" name="appid" value="{{ isset($application) ? $application->id : "" }}" /> 
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if(!is_null($vfields) && !empty($vfields))
                              @foreach($vfields as $field => $value )
                                <div class="form-group">
                                  <label>
                                    {{$value['display']}} 
                                    @if($value['description'] != "") 
                                    <span data-toggle="tooltip" data-original-title="{{$value['description']}}" class="label label-info">?</span>
                                    @endif
                                  </label>
                                  @if ($value['type'] == 'option')
                                  <select class="form-control" name="{{$value['name']}}">
                                    @foreach($value['options'] as $items)
                                      <option value="{{$items['name']}}" {{ $items['name'] == $value['value'] ? 'selected="selected"' : '' }}>{{$items['display']}}</option>
                                    @endforeach
                                  </select>
                                  @elseif($value['type'] == 'date range')
                                    <button class="btn daterange-btn" type="button">Select Date</button>
                                  @else 
                                    <input type="text" name="{{$value['name']}}" class="form-control" value="{{$value['value']}}" />
                                  @endif
                                </div>
                              @endforeach
                            @endif
                            <div class="form-group">
                              <button class="btn btn-primary" type="submit">SAVE CHANGES</button>
                            </div>
                          </form>
                          <hr/>
                          <h3>Display <small>snippet</small></h3>
                          <form method="POST" action="{{URL('/display/update')}}">
                            <input type="hidden" name="appid" value="{{ isset($application) ? $application->id : "" }}" /> 
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if(!is_null($vfields) && !empty($vfields))
                              @foreach($dfields as $field => $value )
                                <div class="form-group">
                                  <label>
                                    {{$value['display']}} 
                                    @if($value['description'] != "") 
                                    <span data-toggle="tooltip" data-original-title="{{$value['description']}}" class="label label-info">?</span>
                                    @endif
                                  </label>
                                  @if ($value['type'] == 'option')
                                  <select class="form-control" name="{{$value['name']}}">
                                    @foreach($value['options'] as $items)
                                      <option value="{{$items['name']}}" {{ $items['name'] == $value['value'] ? 'selected="selected"' : '' }}>{{$items['display']}}</option>
                                    @endforeach
                                  </select>
                                  @elseif($value['type'] == 'date range')
                                    <button class="btn daterange-btn" type="button">Select Date</button>
                                  @else 
                                    <input type="text" name="{{$value['name']}}" class="form-control" id="id-{{$value['name']}}" value="{{$value['value']}}" />
                                  @endif
                                </div>
                              @endforeach
                            @endif
                            <div class="form-group">
                              <button class="btn btn-primary" type="submit">SAVE CHANGES</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="settings">
                      <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-xs-12">
                          <form method="POST" action="{{ isset($application) ? URL('/application/' . $application->id) : URL('/application') }}">
                            <input name="_method" type="hidden" value="PUT" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ isset($application) ? $application->id : old('id') }}">
                            <div class="form-group">
                                <label>Application Name</label>
                                <input value="{{ isset($application) ? $application->name : old('Name') }}" type="text" name="name" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <input value="{{ isset($application) ? $application->description : old('Description') }}" type="text" name="description" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Domain</label>
                                <input value="{{ isset($application) ? $application->domain : old('Domain') }}" type="text" name="domain" class="form-control" />
                            </div>
                            <div class="form-group">
                              <button class="btn btn-primary" type="submit">SAVE CHANGES</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="apikeys">
                      <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-xs-12">
                          <form method="POST" action="{{URL('/application/regkey')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <table style="width: 100%; font-size: 18px;">
                              <tr><td colspan="2">&nbsp;</td></tr>
                              <tr>
                                <td>API KEYS</td>
                                <td>{{ isset($application) ? $application->key : '' }}</td>
                              </tr>
                              <tr><td colspan="2">&nbsp;</td></tr>
                              <tr><td colspan="2"><button class="btn btn-default" type="submit"><i class="fa fa-refresh"></i> Regenerate API Key</button></td></tr>
                            </table>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @else
        <div class="col-xs-12">
          <div style="margin: 100px auto; width: 80%; border: 3px dashed #FEFEFE; padding: 20px; text-align: center;">
            <h3>Create one!</h3> <button class="btn btn-primary" onclick="window.location='{{url('/application/create')}}'"><i class="fa fa-plus"></i>  Add New Application</button>
          </div>
        </div> 
      </div>

      @endif
      <!-- /.row (main row) -->
    </section>
    <!-- add profile modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalAdd">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">New Profile</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label>Profile Name</label>
              <input type="text" class="form-control" />
            </div>
            <div class="form-group">
              <label>Website</label>
              <input type="text" class="form-control" />
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
            <button type="button" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('sidebar')
<aside class="control-sidebar control-sidebar-dark">
  <div style="padding: 10px; border-bottom: 2px solid #181F23;"><button class="btn btn-success btn-block">SAVE CHANGES</button></div>
  <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-tv"></i></a></li>
  </ul>
  
    <form method="POST" action="/snippet/update">
  <div class="tab-content">
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Snippets</h3>
        
      </div>
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <h3 class="control-sidebar-heading">Snippets</h3>
        @if(!is_null($vfields) && !empty($vfields))
            @foreach($vfields as $field => $value )
              <div class="form-group">
                <label>{{$value['display']}}</label>
                <input type="text" name="{{$value['name']}}" class="form-control" />
              </div>
            @endforeach
        @endif
      </div>
  </div>
    </form>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
@stop

@section('page-script')
<!-- bootstrap color picker -->
<script src="{{ URL::asset('/plugins/colorpicker/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{ URL::asset('/public/'.$application->key)}}.js"></script>
<!-- Select2 -->
<script src="{{ URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript">
	$(function () {
    $(".select2").select2();
		$('.daterange-btn').daterangepicker(
    	    {
    	      ranges: {
    	        'Today': [moment(), moment()],
    	        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    	        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    	        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    	        'This Month': [moment().startOf('month'), moment().endOf('month')],
    	        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	      },
    	      startDate: moment().subtract(29, 'days'),
    	      endDate: moment()
    	    },
    	    function (start, end) {
    	      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    	    }
    	);
		//Colorpicker
	});
    	$(".my-colorpicker1").colorpicker();
var interval;
var counter = 0;
  $( document ).ready(function() {
    interval = setInterval("loadPush()", 1000);
  });
  function loadPush(){
    if (counter == 1) {
      pushQuery();
    }
    counter += 1;
  }
  $('#emb-try').click(function () {
    pushQuery();
  });
  $( "#resizable" ).resizable({
    aspectRatio: 16 / 9,
    minHeight: 135,
    minWidth: 240,
    resize : function(event, ui) {
      $("#id-height").val(ui.size.height);
      $("#id-width").val(ui.size.width);
    }
  });
  $("#id-width").on('keyup keypress blur change', function(e) {
      var x = 9/16 * $("#id-width").val(); 
    	$("#id-height").val(x);
    });
    $("#id-height").on('keyup keypress blur change', function(e) {
      var x = 16/9 * $("#id-height").val(); 
    	$("#id-width").val(x);
    });
    $('input[name=embQueryMaster]').on('keyup keypress blur change', function(e) {
    	counter = 0;
    });
    function pushQuery(){
      $('#embSelMaster').children().text($('input[name=embQueryMaster]').val());
      embideo.push({
          type:'default'
      });
    }
	</script>
@stop