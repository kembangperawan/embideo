@extends('layouts.default')
@section('content')
    <!-- Main content -->
    <section class="content" style="background: #DDD;">
      <!-- Small boxes (Stat box) -->
      <div class="row">
      	<div class="col-lg-6 col-lg-offset-3 col-md-offset-2 col-md-8 col-xs-12">
      		<div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Basic Settings</h3>
              </div>
              <div class="box-body">
                <!-- Date -->
                <div class="form-group">
                  <label>Website:</label>
                    <input type="text" class="form-control">
                  <!-- /.input group -->
                </div>
              </div>
              <!-- /.box-body -->
            </div>
      	</div>
      	<div class="col-xs-12 text-right">
      		<div class="form-group"><button class="btn btn-primary">SAVE SETTINGS</button></div>
      	</div>
      </div>
      <!-- /.row (main row) -->
    </section>
@stop
