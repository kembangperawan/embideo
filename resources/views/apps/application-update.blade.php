@extends('layouts.default')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
      	<div class="col-xs-12">
            <h3>{{ isset($application) ? "Update" : "Add New" }} Application</h3>
            <form method="POST" action="{{ url(isset($application) ? '/application/' . $application->id :'/application') }}" accept-charset="UTF-8">
                    <input name="_method" type="hidden" value="{{ isset($application) ? 'PUT': 'POST' }}" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label>Application Name</label>
                    <input value="{{ isset($application) ? $application->name : old('Name') }}" type="text" name="name" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <input value="{{ isset($application) ? $application->description : old('Description') }}" type="text" name="description" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Domain</label>
                    <input value="{{ isset($application) ? $application->domain : old('Domain') }}" type="text" name="domain" class="form-control" />
                </div>
                <input type="submit" name='publish' class="btn" value="Save"/>
                <a href="{{url('/application')}}" class="btn btn-link">Cancel</a>
            </form>
        </div>
    </div>
</section>
@stop