@extends('layouts.default')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
      	<div class="col-xs-12">
            <div class="box box-default">
                <div class="box-body">
                    <h3>{{ isset($blacklist) ? "Update" : "Add New" }} blacklist</h3>
                    @if($errors->has())
                    <div class="alert alert-danger">{{$errors->first()}}</div>
                    @endif
                    <form method="POST" action="{{ url(isset($blacklist) ? '/blacklist/' . $blacklist->id : '/blacklist') }}" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="{{ isset($blacklist) ? 'PUT': 'POST' }}" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!--<div class="form-group">
                            <label>Query</label>
                            <input value="{{ isset($blacklist) ? $blacklist->query : old('query') }}" type="text" name="query" class="form-control" />
                        </div>
                        -->
                        <div class="form-group">
                            <label>Video URL</label>
                            <input value="{{ isset($blacklist) ? $blacklist->video_id : old('video_id') }}" type="text" name="video_id" class="form-control" />
                            <small>eg: https://www.youtube.com/watch?v=WhsoUES5MkU</small>
                        </div>
                        <div class="form-group">
                            <label>Profile</label>
                            <select class="form-control select2" onchange="this.form.submit()"  name="application_id">
                            <option value="0">-- All --</option>
                            @if(!is_null($application_list))
                            @foreach($application_list as $field => $value )
                                <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                            @endif
                            </select>
                        </div>
                        <input type="submit" class="btn btn-primary" value="SAVE"/>
                        <input type="button" onclick="window.location='{{url('/blacklist')}}'" name='save' class="btn btn-default" value="Cancel" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop