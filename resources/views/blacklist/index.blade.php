@extends('layouts.default')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">
                        <div class="form-group text-right">
                            <a href="{{url('/blacklist/create')}}"><i class="fa fa-plus"></i>  Add New Blacklist</a>    
                        </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align: center;" width="50px;">Active?</th>
                                    <th style="text-align: center;" width="200px">Video</th>
                                    <th style="text-align: center;">Rules</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($blacklists as $key => $value)
                                <tr>
                                    <td style="text-align: center;">
                                        <input type="checkbox" name="is_active" {{ $value->is_active ? "checked='checked'" : "" }}
                                        <form method="POST" style="display: inline;" action="{{ url('/blacklist/'.$value->id) }}" accept-charset="UTF-8"
                                        onsubmit="return window.confirm('Delete this row?');">
                                            <input name="_method" type="hidden" value="DELETE" />
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-link" name='publish' class="link" value="delete"/>
                                        </form>
                                    </td>
                                    <td style="text-align: center;">
                                        <iframe width="150" height="85" src="https://www.youtube.com/embed/{{ $value->video_id }}"></iframe>
                                    </td>
                                    <td>
                                        @if ($value->type==1)
                                        <div>Application : {{ $value->application_id == 0 ? "All" : $value->application_id }}</div>
                                        @else
                                        <div><b>Query :</b> {{ $value->query }}</div>
                                        <div>Application : {{ $value->application_id }}</div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="form-group text-right">
                            <a href="{{url('/blacklist/create')}}"><i class="fa fa-plus"></i>  Add New Blacklist</a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop