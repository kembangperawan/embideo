@extends('layouts.default')
@section('content')
    <section class="content">
      <div class="row">
      	<div class="col-xs-12">
          <div class="box box-default">
            <div class="box-body">
                <div class="form-group">
                    <button class="btn btn-primary" onclick="window.location='{{url('/transaction/create')}}'"><i class="fa fa-plus"></i>  Add New Transaction</button>    
                </div>
                @if(Session::has('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('message')}}
                </div>
                @endif
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Transaction ID</th>
                            <th>User ID</th>
                            <th>Plan ID</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Expected Start Date</th>
                            <th>Expected End Date</th>
                            <th>Price</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($transactionlist as $key => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->user_id }}</td>
                            <td>{{ $value->plan_id }}</td>
                            <td>{{ $value->start_date }}</td>
                            <td>{{ $value->end_date }}</td>
                            <td>{{ $value->expected_start_date }}</td>
                            <td>{{ $value->expected_end_date }}</td>
                            <td>{{ $value->price }}</td>
                            <td><a class="btn" href="{{URL::to('/transaction/generateinvoice/'. $value->id)}}">Generate Invoice</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </div>
    </section>
@stop