@extends('layouts.default')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <h3>{{ isset($transactionplan) ? "Update" : "Add New" }} Transaction</h3>
            <form method="POST" action="{{ url(isset($transactionplan) ? '/transaction/' . $transactionplan->id : '/transaction') }}" accept-charset="UTF-8">
                <input name="_method" type="hidden" value="{{ isset($transactionplan) ? 'PUT': 'POST' }}" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label>User ID</label>
                    <input required="required" type="text" name="user_id" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Plan ID</label>
                    <input required="required" type="text" name="plan_id" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Operation</label>
                    <input required="required" type="text" name="operation" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Application Limit</label>
                    <input required="required" type="text" name="apps_limit" class="form-control" />
                </div>
                <div class="checkbox">
                    <input type="hidden" name="customize" value="0">
                    <label class="checkbox">{!! Form::checkbox('customize', 1, isset($transactionplan) && $transactionplan->customize ?  true : false ) !!} Customizeable</label>
                </div>
                <div class="checkbox">
                    <input type="hidden" name="blacklist" value="0">
                    <label class="checkbox">{!! Form::checkbox('blacklist', 1, isset($transactionplan) && $transactionplan->blacklist ?  true : false ) !!} Blacklist</label>
                </div>
                <div class="checkbox">
                    <input type="hidden" name="action_tracking" value="0">
                    <label class="checkbox">{!! Form::checkbox('action_tracking', 1, isset($transactionplan) && $transactionplan->action_tracking ?  true : false ) !!} Action Tracking</label>
                </div>
                <div class="checkbox">
                    <input type="hidden" name="phone_support" value="0">
                    <label class="checkbox">{!! Form::checkbox('phone_support', 1, isset($transactionplan) && $transactionplan->phone_support ?  true : false ) !!} Phone Support</label>
                </div>
                <div class="form-group">
                    <label>Report</label>
                    <input required="required" type="text" name="report" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Email Support</label>
                    <input required="required" type="text" name="email_support" class="form-control" />
                </div>
                <div class="form-group" id="divAdditionalFeature">
                    
                </div>
                <div class="form-group">
                    <button class="btn btn-info" onclick="createNewField();">Add more field</button>
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" required="required" name="start_date" class="form-control pull-right" id="start_date">
                    </div>
                </div>
                <div class="form-group">
                    <label>End Date</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" required="required" name="end_date" class="form-control pull-right" id="end_date">
                    </div>
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <input required="required" type="text" name="price" class="form-control" />
                </div>
                <input type="submit" name='publish' class="btn btn-primary" value="SAVE"/>
                <input type="button" onclick="window.location='{{url('/transaction')}}'" name='save' class="btn btn-default" value="Cancel" />
            </form>
        </div>
    </div>
</section>
@stop
@section('page-script')
<script type="text/javascript">
    $('#start_date').datepicker({
      autoclose: true
    });
    $('#end_date').datepicker({
      autoclose: true
    });
    function createNewField(){
        $('#divAdditionalFeature').append('<div class="col-xs-6"><label>Key</label><input type="text" class="form-control" /></div><div class="col-xs-6"><label>Value</label><input type="text" class="form-control" /></div><div style="clear: both;"></div>');
    }
</script>
@stop