@extends('layouts.default')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
      	<div class="col-xs-12">
          <div class="box box-default">
            <div class="box-body">
                <h3>{{ isset($plan) ? "Update" : "Add New" }} Plan</h3>
                <form method="POST" action="{{ url(isset($plan) ? '/plan/' . $plan->id : '/plan') }}" accept-charset="UTF-8">
                    <input name="_method" type="hidden" value="{{ isset($plan) ? 'PUT': 'POST' }}" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>Name</label>
                        <input required="required" value="{{ isset($plan) ? $plan->name : old('Name') }}" type="text" name="name" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input required="required" value="{{ isset($plan) ? $plan->description : old('description') }}" type="text" name="description" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Operation</label>
                        <input required="required" value="{{ isset($plan) ? $plan->operation : old('operation') }}" type="text" name="operation" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Application Limit</label>
                        <input required="required" value="{{ isset($plan) ? $plan->apps_limit : old('apps_limit') }}" type="text" name="apps_limit" class="form-control" />
                    </div>
                    <div class="checkbox">
                        <input type="hidden" name="customize" value="0">
                        <label class="checkbox">{!! Form::checkbox('customize', 1, isset($plan) && $plan->customize ?  true : false ) !!} Customizeable</label>
                    </div>
                    <div class="checkbox">
                        <input type="hidden" name="blacklist" value="0">
                        <label class="checkbox">{!! Form::checkbox('blacklist', 1, isset($plan) && $plan->blacklist ?  true : false ) !!} Blacklist</label>
                    </div>
                    <div class="checkbox">
                        <input type="hidden" name="action_tracking" value="0">
                        <label class="checkbox">{!! Form::checkbox('action_tracking', 1, isset($plan) && $plan->action_tracking ?  true : false ) !!} Action Tracking</label>
                    </div>
                    <div class="form-group">
                        <label>Report</label>
                        <input required="required" value="{{ isset($plan) ? $plan->report : old('report') }}" type="text" name="report" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Email Support</label>
                        <input required="required" value="{{ isset($plan) ? $plan->email_support : old('email_support') }}" type="text" name="email_support" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Phone Support</label>
                        <input required="required" value="{{ isset($plan) ? $plan->phone_support : old('phone_support') }}" type="text" name="phone_support" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input required="required" value="{{ isset($plan) ? $plan->price : old('Price') }}" type="text" name="price" class="form-control" />
                    </div>
                    <input type="submit" name='publish' class="btn btn-primary" value="SAVE"/>
                    <input type="button" onclick="window.location='{{url('/plan')}}'" name='save' class="btn btn-default" value="Cancel" />
                </form>
            </div>
          </div>
        </div>
    </div>
</section>
@stop