@extends('layouts.default')
@section('content')
    <section class="content">
        <div class="row">
      	    <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">
                        <div class="form-group">
                            <button class="btn btn-primary" onclick="window.location='{{url('/plan/create')}}'"><i class="fa fa-plus"></i>  Add New Plan</button>    
                        </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>Plan</td>
                                    <td>Price</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($plan_list as $key => $value)
                                <tr>
                                    <td>
                                        <dl>
                                            <dt>{{ $value->name }}</dt>
                                            <dd>{{ $value->description }}</dd>
                                        </dl>
                                    </td>
                                    <td>{{ $value->price }}</td>

                                    <!-- we will also add show, edit, and delete buttons -->
                                    <td>
                                        <a class="btn" href="{{ URL::to('/plan/' . $value->id . '/edit') }}">Edit</a>
                                        <form method="POST" style="display: inline;" action="{{ url('/plan/'.$value->id) }}" accept-charset="UTF-8"
                                        onsubmit="return window.confirm('Delete this row?');">
                                            <input name="_method" type="hidden" value="DELETE" />
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-link" name='publish' class="link" value="delete"/>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop