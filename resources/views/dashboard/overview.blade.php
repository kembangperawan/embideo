@extends('layouts.default')
@section('content')
    <section class="content">
      <div class="row">
      @if(Session::has('messageSuccess'))
      <div class="col-xs-12">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('messageSuccess')}}
        </div>
      </div>
      @endif
      @if(Session::has('messageError'))
      <div class="col-xs-12">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('messageError')}}
        </div>
      </div>
      @endif
      @if (isset($currentplan) && $currentplan != null)
        <div class="col-xs-12">
          <h3 style="font-weight: 600; margin: 0 0 20px 0;">Performance <small>LAST 24H</small></h3>
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box info-box-small">
                <span class="info-box-icon bg-gray"><i class="fa fa-gear"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">API Call</span>
                  <span class="info-box-number">{{$apiSummary['apicall']}}</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box info-box-small">
                <span class="info-box-icon bg-gray"><i class="fa fa-play"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Video Played</span>
                  <span class="info-box-number">{{$apiSummary['videoPlayed']}}</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box info-box-small">
                <span class="info-box-icon bg-gray"><i class="fa fa-hourglass-start"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Avg. Video Play Rate</span>
                  <span class="info-box-number">{{$apiSummary['avgVideo']}}%</span>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box info-box-small">
                <span class="info-box-icon bg-gray"><i class="fa fa-film"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Unique Video</span>
                  <span class="info-box-number">{{$apiSummary['uniqueVideo']}}</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xs-12">
          <div class="box box-default">
      			<div class="box-header"><h3 class="box-title" style="font-weight: 600;">Usage since {{$currentplan->start_date}}</h3></div>
      			<div class="box-body">
              <div class="dashboard-panel">
                <h3 class="title"><i class="fa fa-gears"></i> Operation</h3>
                <div class="progress progress-sm">
                  <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                    <span class="sr-only">40% Complete (success)</span>
                  </div>
                </div>
                <div style="float: right;">40%</div>
                <h3>400<span class="small"> / 1000</span></h3>
              </div>
              <div class="dashboard-panel">
                <h3 class="title"><i class="fa fa-database"></i> Application List</h3>
                <ul class="list-detail">
                  @foreach ($application_list as $key => $value)
                    <li>
                      <span><a href="#">{{$value->domain}}</a></span>
                      <span class="count">30%</span>
                    </li>
                  @endforeach
                </ul>
              </div>
              <div class="dashboard-panel">
                <div class="row">
                  <div class="col-xs-1">
                    <span>Plan<br/> <strong style="font-size: 16px;">{{$currentplan->plan->name}}</strong></span>
                  </div>
                  <div class="col-xs-5">
                    <span style="font-size: 32px;">{{$remaining}} days</span>
                    <span style="display: inline-block; font-size: 12px; line-height: 12px;">remaining<br/> until renewal</span>
                  </div>
                  <div class="col-xs-5 col-xs-offset-1 text-right">
                    <a href="{{url('/account/plan')}}" class="btn btn-lg">CHANGE PLAN</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
          <div class="box box-default">
      			<div class="box-header"><h3 class="box-title">Popular Query</h3></div>
      			<div class="box-body">
              <dl>
                <dt><a href="#">Unboxing Asus</a><span style="float: right;">x1000</span></dt>
                <dd>Bhinneka.Com</dd>
                <dt><a href="#">Unboxing Asus</a><span style="float: right;">x1000</span></dt>
                <dd>Bhinneka.Com</dd>
                <dt><a href="#">Unboxing Asus</a><span style="float: right;">x1000</span></dt>
                <dd>Bhinneka.Com</dd>
                <dt><a href="#">Unboxing Asus</a><span style="float: right;">x1000</span></dt>
                <dd>Bhinneka.Com</dd>
              </dl>
            </div>
          </div>
        </div>
        @endif
      </div>
    </section>
@stop
