@extends('layouts.default')
@section('title', 'Account Details')
@section('pageTitle', 'Account')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 col-xs-12">
        <form method="POST" action="{{ isset($application) ? 'http://localhost:9090/application/' . $application->id : 'http://localhost:9090/application' }}" accept-charset="UTF-8">
            <div class="form-group">
                <input name="_method" type="hidden" value="{{ isset($application) ? 'PUT': 'POST' }}" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input required="required" value="{{ isset($application) ? $application->name : old('Name') }}" placeholder="Enter Name here" type="text" name="name" class="form-control" />
                <input required="required" value="{{ isset($application) ? $application->description : old('Description') }}" placeholder="Enter Description here" type="text" name="description" class="form-control" />
                <select name="planId" class="form-control">
                     <option value="0">Please Select</option>
                @foreach($plan_list as $plan)
                    <option value="{{ $plan->id }}">{{$plan->name . ' - Price: ' . $plan->price}}</option>
                @endforeach
                </select>
            </div>
            <input type="submit" name='publish' class="btn btn-success" value="Publish"/>
            <input type="submit" name='save' class="btn btn-default" value="Save Draft" />
            <a href="http://localhost:9090/application">cancel</a>
        </form>
    </div>
  </div>
</section>
<!-- /.content -->
@stop
@section('page-script')
<script>
	$(function () {
		$('#datepicker').datepicker({
      		autoclose: true
    	});
	});
</script>
@stop
