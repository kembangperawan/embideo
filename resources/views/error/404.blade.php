<!DOCTYPE html>
<html>
    <head>
        <title>Embideo - 404 Not Found</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{ URL::asset('/css/dashboard.css') }}">
    </head>
    <body style="background: #F9FAFC; color: #444;">
        <div style="margin: 200px auto; width: 70%; ">
            <div style="width: 50%; float: left; margin-right: 10%;">
                <div style="font-size: 72px; font-weight: bold;">OOPS!</div>
                <div style="font-size: 24px;">The page you were looking for appears to have been moved, deleted or does not exist.
You could go back to where you were or head straight to our home page.</div>
                <div style="font-weight: bold; margin-top: 30px;">error 404: Page not found.</div>
            </div>
            <div style="width: 40%; float: left;">
                <a href="http://embideo.com"><img src="{{ URL::asset('/img/embideo-logo-300.png') }}" alt="embideo" title="embideo" /></a>
            </div>
            <div style="clear: both"></div>
        </div>
    </body>
</html>