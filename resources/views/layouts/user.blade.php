<!DOCTYPE html>
<html>
<head>
  @include('includes.head')
</head>
<body class="hold-transition login-page">
    @yield('content')
@include('includes.footer')
</body>
</html>
