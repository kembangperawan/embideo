@extends('layouts.default')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">
                        <div class="col-md-4 col-xs-12">
                            <form action="{{URL('/analytics/change')}}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label style="font-size: 16px;">Profile&nbsp;</label>
                                <select class="form-control select2" onchange="this.form.submit()"  name="appid">
                                @if(!is_null($application_list))
                                @foreach($application_list as $field => $value )
                                    <option value="{{$value->id}}"{{$value->id == $application->id ? "selected='selected'" : ""}}>{{$value->name}}</option>
                                @endforeach
                                @endif
                                </select>
                            </div>
                            </form>
                        </div>
                        <form action="{{URL('/analytics/getdata')}}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div style="line-height: 27px;">From :</div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="startdate" class="form-control date-picker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="col-xs-6">
                                        <div style="line-height: 27px;">To :</div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="enddate" class="form-control date-picker" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12">
                                <div style="line-height: 26px;">&nbsp;</div>
                                <button class="btn btn-primary btn-block" type="submit">GO!</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="box box-default">
                            <div class="box-header"><h3 class="box-title">Top Query</h3></div>
                            <div class="box-body dl-bordered">
                                <table class="table table-striped">
                                    <tr>
                                        <th width="85%">Query</th>
                                        <th style="text-align: right;">Count</th>
                                    </tr>
                                    @foreach ($response as $key => $value)
                                    <tr>
                                        <td><a href="/analytics/show/">{{$value['Query'] == "" ? '[empty]' : $value['Query']}}</a></td>
                                        <td style="text-align: right;"><small>x</small> {{$value['Count']}}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="box box-default">
                        <div class="box-header"><h3 class="box-title">Top Playing</h3></div>
                            <div class="box-body">
                                <dl>
                                    @foreach ($response as $key => $value)
                                    <dt><a href="{{$value['url']}}">{{$value['Query']}}</a><span style="float: right;">{{$value['Count']}}</span></dt>
                                    <dd>{{$value['application_id']}}</dd>
                                    @endforeach
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('page-script')
<script src="{{ URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{ URL::asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript">
	$(function () {
        $(".select2").select2();
        $('.date-picker').datepicker({
            autoclose: true
        });
    });
</script>
@stop