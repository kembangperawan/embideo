@extends('layouts.default')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">
                        <div class="form-group">
                            <button class="btn btn-primary" onclick="window.location='{{url('/user/create')}}'"><i class="fa fa-plus"></i>  Add New User</button>    
                        </div>
                        @if(Session::has('message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('message')}}
                        </div>
                        @endif
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Website</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $value)
                                <tr>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->website }}</td>
                                    <td>
                                        <a class="btn" href="{{ URL::to('/user/' . $value->id . '/edit') }}">Edit</a>
                                        <form method="POST" style="display: inline;" action="{{ url('/user/'.$value->id) }}" accept-charset="UTF-8"
                                        onsubmit="return window.confirm('Delete this row?');">
                                            <input name="_method" type="hidden" value="DELETE" />
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-link" name='publish' class="link" value="delete"/>
                                        </form>
                                        <a class="btn" href="{{URL::to('/user/setadmin/'. $value->id)}}">{{!$value->is_admin ? "Set Admin" : "Remove Admin"}}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop