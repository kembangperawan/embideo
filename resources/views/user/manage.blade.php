@extends('layouts.default')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
      	<div class="col-xs-12">
            <div class="box box-default">
                <div class="box-body">
                    <h3>{{ isset($user) ? "Update" : "Add New" }} user</h3>
                    @if($errors->has())
                    <div class="alert alert-danger">{{$errors->first()}}</div>
                    @endif
                    <form method="POST" action="{{ url(isset($user) ? '/user/' . $user->id : '/user') }}" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="{{ isset($user) ? 'PUT': 'POST' }}" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label>Username/Email</label>
                            <input value="{{ isset($user) ? $user->email : old('email') }}" type="text" name="email" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input value="{{ isset($user) ? $user->name : old('Name') }}" type="text" name="name" class="form-control" />
                        </div>
                        @if (!isset($user))
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" />
                        </div>
                        @endif
                        <input type="submit" name='publish' class="btn btn-primary" value="SAVE"/>
                        <input type="button" onclick="window.location='{{url('/user')}}'" name='save' class="btn btn-default" value="Cancel" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop