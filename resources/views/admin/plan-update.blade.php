<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('admin/plan') }}">plan</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('admin/plan') }}">View All plan</a></li>
            <li><a href="{{ URL::to('admin/plan/create') }}">Create a plan</a>
        </ul>
    </nav>

    <form method="POST" action="{{ isset($plan) ? 'http://localhost:9090/admin/plan/' . $plan->id : 'http://localhost:9090/admin/plan' }}" accept-charset="UTF-8">
        <div class="form-group">
            <input name="_method" type="hidden" value="{{ isset($plan) ? 'PUT': 'POST' }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input required="required" value="{{ isset($plan) ? $plan->name : old('Name') }}" placeholder="Enter Name here" type="text" name="name" class="form-control" />
            <input required="required" value="{{ isset($plan) ? $plan->description : old('description') }}" placeholder="Enter description here" type="text" name="description" class="form-control" />
            <input required="required" value="{{ isset($plan) ? $plan->operations : old('operations') }}" placeholder="Enter number of operations here" type="text" name="operations" class="form-control" />
            <input required="required" value="{{ isset($plan) ? $plan->apps_limit : old('apps_limit') }}" placeholder="Enter number of application quota here" type="text" name="appsLimit" class="form-control" />
            customizable? : 
            <input @if (isset($plan) && $plan->customize === 1) checked="checked" @endif id="radCustomizableYes" type="radio" name="customize" value="yes" /><label for="radCustomizableYes" title="yes">yes</label>
            <input @if (isset($plan) && $plan->customize === 0) checked="checked" @endif id="radCustomizableNo" type="radio" name="customize" value="no" /><label for="radCustomizableNo" title="no" />no</label><br />
            able to blacklist? : 
            <input @if (isset($plan) && $plan->blacklist === 1) checked="checked" @endif id="radBlacklistYes" type="radio" name="blacklist" value="yes" /><label for="radBlacklistYes" title="yes">yes</label>
            <input @if (isset($plan) && $plan->blacklist === 0) checked="checked" @endif id="radBlacklistNo" type="radio" name="blacklist" value="no" /><label for="radBlacklistNo" title="no" />no</label><br />
            enable action tracking? : 
            <input @if (isset($plan) && $plan->action_tracking === 1) checked="checked" @endif id="radActionTrackingYes" type="radio" name="actionTracking" value="yes" /><label for="radActionTrackingYes" title="yes">yes</label>
            <input @if (isset($plan) && $plan->action_tracking === 0) checked="checked" @endif id="radActionTrackingNo" type="radio" name="actionTracking" value="no" /><label for="radActionTrackingNo" title="no" />no</label><br />
enter reports description: <br />
<textarea name="reports" rows="5" cols="100">
{{ isset($plan) ? $plan->reports : old('reports') }}
</textarea>
<br />
enter email support description: <br />
<textarea name="emailSupport" rows="5" cols="100">
{{ isset($plan) ? $plan->email_support : old('email_support') }}
</textarea>
<br />
            enable phone support? : 
            <input  @if (isset($plan) && $plan->phone_support === 1) checked="checked" @endif id="radPhoneSupportYes" type="radio" name="phoneSupport" value="yes" /><label for="radPhoneSupportYes" title="yes">yes</label>
            <input @if (isset($plan) && $plan->phone_support === 0) checked="checked" @endif id="radPhoneSupportNo" type="radio" name="phoneSupport" value="no" /><label for="radPhoneSupportNo" title="no" />no</label><br />
            <input required="required" value="{{ isset($plan) ? $plan->price : old('price') }}" placeholder="Enter price here" type="text" name="price" class="form-control" />
        </div>
        <input type="submit" name='publish' class="btn btn-success" value="Publish"/>
    </form>
</body>
</html>
