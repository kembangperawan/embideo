<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('admin/plan') }}">admin plan</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('admin/plan') }}">View All plan</a></li>
            <li><a href="{{ URL::to('admin/plan/create') }}">Create a plan</a>
        </ul>
    </nav>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>description</td>
                <td>operations</td>
                <td>apps_limit</td>
                <td>customize</td>
                <td>blacklist</td>
                <td>action_tracking</td>
                <td>reports</td>
                <td>email_support</td>
                <td>phone_support</td>
                <td>price</td>
            </tr>
        </thead>
        <tbody>
        @foreach($plan_list as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->operations }}</td>
                <td>{{ $value->apps_limit }}</td>
                <td>{{ $value->customize }}</td>
                <td>{{ $value->blacklist }}</td>
                <td>{{ $value->action_tracking }}</td>
                <td>{{ $value->reports }}</td>
                <td>{{ $value->email_support }}</td>
                <td>{{ $value->phone_support }}</td>
                <td>{{ $value->price }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <a class="btn btn-small btn-success" href="{{ URL::to('admin/plan/' . $value->id) }}">Show this plan</a>
                    <a class="btn btn-small btn-info" href="{{ URL::to('admin/plan/' . $value->id . '/edit') }}">Edit this plan</a>
                    <form method="POST" action="{{ 'http://localhost:9090/admin/plan/'.$value->id }}" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" name='publish' class="btn btn-success" value="delete"/>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>

