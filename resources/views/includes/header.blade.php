<header class="main-header">
  <a href="/" class="logo">
    <span class="logo-mini"><b>E</b></span>
    <span class="logo-lg"><b>E</b>Admin</span>
  </a>
  <nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        @if (!Auth::guest())
          @if (Auth::user()->is_admin)
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-users"></i>{{ LogasUser() != null && LogasUser()->id != Auth::user()->id ? " Loged as ".LogasUser()->email : ""}}</a>
            <ul class="dropdown-menu" id="ddlUser">
              <li class="header">Log as :</li>
              @foreach (getAllUser() as $key => $value)
              <li><a href="javascript:void(0)" data-userid="{{$value->id}}">{{$value->email}}</a></li>
              @endforeach 
            </ul>
          </li>
          @endif
          <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a>
            <ul class="dropdown-menu">
              <li><a href="{{ url('/account/detail') }}">Edit Profile</a></li>
            </ul>
          </li>
          <li><a href="{{ url('/auth/logout') }}" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="fa fa-power-off"></i></a></li>
        @endif
      </ul>
    </div>
    <form style="display: none;" method="POST" action="{{URL('user/log-as')}}" id="formLogas">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="logasID" />
    </form>
  </nav>
</header>