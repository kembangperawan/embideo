<script src="{{ URL::asset('plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ URL::asset('/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ URL::asset('/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ URL::asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::asset('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ URL::asset('/plugins/knob/jquery.knob.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ URL::asset('/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ URL::asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ URL::asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('/js/app.js') }}"></script>
@if (!Auth::guest() && Auth::user()->is_admin)
<script type="text/javascript">
  $('#ddlUser li a').click(function(){
    var id =$(this).attr("data-userid");
    $('input[name=logasID]').val(id);
    $('#formLogas').submit();
  });
  $('#formLogas').on( 'submit', function() {
      $.ajax({
        url: $(this).prop('action'),
        type: "POST",
        data: { 
          "_token": $(this).find('input[name=_token]').val(),
          "logasID": $(this).find('input[name=logasID]').val() 
              },
        success:function(data){
          var response = jQuery.parseJSON(data);
          alert(response.message);
          location.reload();
        },error:function(){ 
            alert("Failed to execute. Please refresh the page and try again.");
        }
    });
    return false;
  });
</script>
@endif