@extends('layouts.user')

<!-- Main Content -->
@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="http://embideo.com"><img src="{{ URL::asset('/img/embideo-logo.png') }}" width="100px;" alt="embideo" title="embideo" /></a>
    </div>
    @if(Session::has('messageSuccess'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('messageSuccess')}}
    </div>
    @endif
    @if(Session::has('messageError'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('messageError')}}
    </div>
    @endif
    <div class="login-box-body">
        <p class="login-box-msg">Reset Password</p>
        <form class="form" role="form" method="POST" action="{{ url('/auth/password/email') }}">
            {!! csrf_field() !!}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" placeholder="username or email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection
