@extends('layouts.user')

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="http://embideo.com"><img src="{{ URL::asset('/img/embideo-logo.png') }}" width="100px;" alt="embideo" title="embideo" /></a>
    </div>
    @if(Session::has('messageSuccess'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('messageSuccess')}}
    </div>
    @endif
    @if(Session::has('messageError'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('messageError')}}
    </div>
    @endif
    <div class="login-box-body">
       <form role="form" method="POST" action="{{ url('auth/login') }}">
            {!! csrf_field() !!}
            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" placeholder="email" value="{{ old('email') }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-8">
                <div class="checkbox">
                    <label> 
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
        </form>
        <a class="btn btn-link" href="{{ url('/auth/password/reset') }}">Forgot Your Password?</a>
        <a class="btn btn-link" href="{{ url('/auth/register') }}">Create new account?</a> 
    </div>
</div>
@endsection
