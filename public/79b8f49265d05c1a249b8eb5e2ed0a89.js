window.embideo = window.embideo || function () {
	var _const = {
		ele : {
			id: "val"
		}, 
		container : {
			id: "container"
		}, 
		setting : {
			"type":"video",
			"maxResults":1
		},
		api : "79b8f49265d05c1a249b8eb5e2ed0a89",
		ip: ""
	}

	function emb_q(url) {
	    var callbackName = 'cb_' + Math.round(100000 * Math.random());
	    window[callbackName] = function(data) {
	        delete window[callbackName];
	        document.body.removeChild(script);
	    };

	    var dump = document.getElementById('script_' + _const.api);
	    dump != null && dump.parentNode.removeChild(dump);	    

	    var script = document.createElement('script');
	    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
	    script.id = 'script_' + _const.api;
	    document.body.appendChild(script);
	}

	function emb_ip(url, f) {
		var callbackName = 'cb_' + Math.round(100000 * Math.random());
	    window[callbackName] = function(data) {
	        delete window[callbackName];
	        document.body.removeChild(script);
	        _const.ip = data.ip;
	        f();   
	    };

	    var dump = document.getElementById('script_' + _const.api + '_ip');
	    dump != null && dump.parentNode.removeChild(dump);	    

	    var script = document.createElement('script');
	    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
	    script.id = 'script_' + _const.api + '_ip';
	    document.body.appendChild(script);
	}

	function init() {
		emb_ip('https://api.ipify.org?format=jsonp', function () {
			var q = document.getElementById(_const.ele.id).value;
			var a = ''; 
			a += encodeURIComponent(JSON.stringify({
				d: {
					q : q,
					a : _const.api,
					s : _const.setting,
					u : '',
					i : _const.ip
				}
			}));

			emb_q('http://localhost:9999/v1/' + a);			
		});	
	}

	function pr(data) {
		var obj = document.createElement("iframe");
	    obj.width=420;
	    obj.height=315;
	    obj.src="https://www.youtube.com/embed/" + data.data[0].ret[0].id;
	    obj.frameborder=0;
	    obj.allowfullscreen;
	    document.getElementById(_const.container.id).appendChild(obj);
	}

	return documentOnLoad = window.embideo && window.embideo.documentOnLoad ? window.embideo.documentOnLoad : function (t) {
		t();
	}, {
		documentOnLoad: documentOnLoad,
		init: init,
		pr : pr
	};
}(), embideo.documentOnLoad(function () {
	embideo.init();
});