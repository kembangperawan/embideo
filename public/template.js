window.embideo = window.embideo || function () {
	var _const = {
		ele : {
			type : "{{selector_type}}",
			value: "{{selector_value}}"
		}, 
		container : {
			type : "{{container_type}}",
			value : "{{container_value}}"
		}, 
		video_setting : "{{video_settings}}",
		display_setting : "{{display_settings}}",
		query_setting : {
			prefix : "{{prefix_setting}}",
			sufix : "{{sufix_setting}}"
		},
		api : "{{app_key}}"
	};

	var ajax = {};
	ajax.x = function () {
	    if (typeof XMLHttpRequest !== 'undefined') {
	        return new XMLHttpRequest();
	    }
	    var versions = [
	        "MSXML2.XmlHttp.6.0",
	        "MSXML2.XmlHttp.5.0",
	        "MSXML2.XmlHttp.4.0",
	        "MSXML2.XmlHttp.3.0",
	        "MSXML2.XmlHttp.2.0",
	        "Microsoft.XmlHttp"
	    ];

	    var xhr;
	    for (var i = 0; i < versions.length; i++) {
	        try {
	            xhr = new ActiveXObject(versions[i]);
	            break;
	        } catch (e) {
	        }
	    }
	    return xhr;
	};

	ajax.send = function (url, callback, method, data, async) {
	    if (async === undefined) {
	        async = true;
	    }
	    var x = ajax.x();
	    x.open(method, url, async);
	    x.onreadystatechange = function () {
	        if (x.readyState == 4) {
	            callback(x.responseText)
	        }
	    };
	    if (method == 'POST') {
	        x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	    }
	    x.send(data)
	};

	ajax.get = function (url, data, callback, async) {
	    var query = [];
	    for (var key in data) {
	        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	    }
	    ajax.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async)
	};

	ajax.post = function (url, data, callback, async) {
	    var query = [];
	    for (var key in data) {
	        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	    }
	    ajax.send(url, callback, 'POST', query.join('&'), async)
	};

	function push(data) {		
		if(defined(data, "type")) {
			ajax.get('https://api.ipify.org?format=json', '', function(e) {
				var elvalue = _const.cases[_const.ele.type.toLowerCase()](_const.ele.value).tagName == "INPUT" ? 
					document.getElementById(_const.ele.value).value : document.getElementById(_const.ele.value).innerHTML;
				_const['ip'] = JSON.parse(e).ip;				
				_const['q'] = data.type == 'default' ? elvalue : defined(data, "q") && data.q != '' ? data.q : 'embideo';
				ajax.get('SERVER_HOSTNAME' + "v1/" + encodeURIComponent(JSON.stringify({
					param : {
						q : _const.query_setting.prefix + ' ' + _const['q'] + ' ' + _const.query_setting.sufix,
						api : _const.api,
						vsetting : _const.video_setting,
						url : document.location.href.replace(/\//ig, '\\'),
						ip : _const.ip,
						kind : 'initial'
					}				
				})), '', function (data) {
					pr(JSON.parse(data));
				});
			});
			
		}
	}

	function defined(target, path) {
	    if (typeof target != 'object' || target == null) {
	        return false;
	    }

	    var parts = path.split('.');

	    while(parts.length) {
	        var branch = parts.shift();
	        if (!(branch in target)) {
	            return false;
	        }

	        target = target[branch];
	    }

	    return true;
	}

	function init() {
		//create element
		var div = document.createElement('div')
			, up = document.createElement('span')
			, down = document.createElement('span')
			, prev = document.createElement('span')
			, next = document.createElement('span')
			, title = document.createElement('div')
			, coll = document.createElement('div')
			, videoControl = document.createElement("div")
			, videoContainer = document.createElement("div");
		var controlStyle = "padding: 5px; background: #FFF; cursor: pointer;";
		
		//set attribute
		_const['player_id'] = 'emb_' + Math.round(100000 * Math.random());
		div.setAttribute("id", _const.player_id);
		coll.setAttribute("style", "background: " + _const.display_setting['backgroundcolor'] + "; padding: 10px;");
		title.setAttribute("style", "font-size: 20px; font-weight: bold; padding-bottom: 10px; margin-bottom: 10px; border-bottom: 1px solid #DDD;");
		videoContainer.setAttribute("style", "width: " + _const.display_setting.width + "px; margin: auto;");
		videoControl.setAttribute("style", "padding: 10px; background: #FFF; margin: 5px 0;");
		prev.setAttribute("style", controlStyle);
		next.setAttribute("style", controlStyle);
		up.setAttribute("style", controlStyle);
		down.setAttribute("style", controlStyle);

		//overwrite html
		prev.innerHTML = "<i class='fa fa-step-backward'></i> Prev";
		next.innerHTML = "<i class='fa fa-step-forward'></i> Next";
		up.innerHTML = "Up";
		down.innerHTML = "Down";

		videoControl.appendChild(title);
		videoControl.appendChild(prev);
		videoControl.appendChild(next);
		videoControl.appendChild(up);
		videoControl.appendChild(down);
		
		videoContainer.appendChild(div);
		videoContainer.appendChild(videoControl);

		coll.appendChild(videoContainer);

		//up to constant
		_const['flag'] = 0;
		_const['collection'] = {
			'up' : up,
			'down' : down,
			'prev' : prev,
			'next' : next,
			'title' : title,
			'coll' : coll
		};
		_const['player_container'] = div;
		_const['video'] = {
			'videoControl' : videoControl,
			'videoContainer' : videoContainer
		};
		_const['cases'] = {
			"id" : function (s) {
				var val = document.getElementById(s);
				return val;
			},
			"class" : function (s) {
				var val = document.getElementsByClassName(s)[0];
				return val;
			}, 
			"tag" : function (s) {
				var val = document.getElementsByTagName(s)[0];
				return val;
			}
		};

		_const.cases[_const.container.type.toLowerCase()](_const.container.value).style.display = 'none';
		_const.cases[_const.container.type.toLowerCase()](_const.container.value).appendChild(coll);		
	}

	function pr(data) {	
		/*
			id => data.data[0].ret[_const['flag']].id
			title => data.data[0].ret[_const['flag']].title
			publishedAt => data.data[0].ret[_const['flag']].publishedAt
			channelTitle => data.data[0].ret[_const['flag']].channelTitle
			description => data.data[0].ret[_const['flag']].description
			thumbnails => data.data[0].ret[_const['flag']].thumbnails
		*/
		function reload() {
			data.data[0].ret.length > 0 ?			
			_const.player.cueVideoById({
				videoId: data.data[0].ret[_const['flag']].id
			}) : "";
			_const.collection.title.innerHTML = data.data[0].ret[_const['flag']].title;
		}

		function change(count) {
			var pos = _const['flag'] + count;
			_const['flag'] = pos < 0 ? data.data[0].ret.length - 1 : pos > data.data[0].ret.length - 1 ? 0 : pos;
			data.data[0].ret.length > 0 ?
			_const.player.cueVideoById({
				videoId: data.data[0].ret[_const['flag']].id
			}) : "";
			ajax.get('SERVER_HOSTNAME' + "v1/" + encodeURIComponent(JSON.stringify({
				param : {
					q : _const.query_setting.prefix + ' ' + _const['q'] + ' ' + _const.query_setting.sufix,
					api : _const.api,
					url : document.location.href.replace(/\//ig, '\\'),
					ip : _const.ip,
					vid : data.data[0].ret[_const['flag']].id,
					pvid : data.data[0].ret[0].id,
					position: _const['flag'] + 1,
					kind : 'event'
				}				
			})), '', function(){});
			_const.collection.title.innerHTML = data.data[0].ret[_const['flag']].title;
		}

		function vote(state) {
			ajax.get('SERVER_HOSTNAME' + "v1/" + encodeURIComponent(JSON.stringify({
				param : {
					q : _const.query_setting.prefix + ' ' + _const['q'] + ' ' + _const.query_setting.sufix,
					api : _const.api,
					url : document.location.href.replace(/\//ig, '\\'),
					ip : _const.ip,
					vid : data.data[0].ret[_const['flag']].id,
					eid : data.e_id,
					vote : state,
					kind : 'vote'
				}				
			})), '', function(){});
		}

		function onYouTubeIframeAPIReady() {
			_const['player'] = new YT.Player(_const.player_id, {
				height: _const.display_setting.height,
				width: _const.display_setting.width,
				videoId: data.data[0].ret[_const['flag']].id,
				events: {
					'onReady': onPlayerReady,
					'onStateChange': onPlayerStateChange
				}
			});
			_const.collection.title.innerHTML = data.data[0].ret[_const['flag']].title;
		}

		function onPlayerReady(event) {
			_const.cases[_const.container.type.toLowerCase()](_const.container.value).style.display = 'block';
		}

		var done = false;
		function onPlayerStateChange(event) {
			ajax.get('SERVER_HOSTNAME' + "v1/" + encodeURIComponent(JSON.stringify({
				param : {
					api : _const.api,
					url : document.location.href.replace(/\//ig, '\\'),
					ip : _const.ip,
					vid : data.data[0].ret[_const['flag']].id,
					state : _const.player.getPlayerState(),
					duration : _const.player.getCurrentTime(),
					length : _const.player.getDuration(),
					kind : 'player',
					eid : data.e_id
				}
			})), '', function(){});
		}

		function stopVideo() {
			_const.player.stopVideo();
		}
		
		//event declaration
		_const.collection.prev.addEventListener('click', function() {
			change(-1);
		});
		_const.collection.next.addEventListener('click', function() {
			change(1);
		});
		_const.collection.up.addEventListener('click', function() {
			vote('up');
		});
		_const.collection.down.addEventListener('click', function() {
			vote('down');
		});

		data.data[0].ret.length > 0 ? _const.player == '' || _const.player == undefined ? onYouTubeIframeAPIReady() : reload() : "";
	}

	return documentOnLoad = window.embideo && window.embideo.documentOnLoad ? window.embideo.documentOnLoad : function (t) {		
		t();
		var tag = document.createElement('script');
	    tag.src = "https://www.youtube.com/iframe_api";
	    tag.async = true;	    
	    var firstScriptTag = document.getElementsByTagName('script')[0];
    	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	}, {
		documentOnLoad: documentOnLoad,
		init: init,
		pr : pr,
		push : push
	};
}(), embideo.documentOnLoad(function () {
	embideo.init();
});


